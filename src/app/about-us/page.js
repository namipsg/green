"use client";
import "./index.css";
import pic1 from "../../../public/img/maleki.jpg";
import Image from "next/image";
import { useTranslation } from "react-i18next";

function About_us() {
  const { t } = useTranslation("about-us");
  return (
    <div id="about">
      <section className="about-us">
        <div className="top-pic">
          <div className="container">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">{t("about-us")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="First-p">
          <div className="container ">
            <div className="row">
              <div className="col-12">
                <div className="First-p-box">
                  <div className="first-p-box-h-box">
                    <div className="col-5">
                      <hr className="hr-first" />
                    </div>
                    <h4 className="col-2 first-p-box-h">
                      {t("first-and-last")}
                    </h4>
                    <div className="col-5">
                      <hr className="hr-first" />
                    </div>
                  </div>
                  <div className="first-p-box-p-box">
                    <p className="first-p-box-p">
                    کمپانی تهویه مطبوع گرین تولیدکننده دستگاه‌های مرکزی و مستقل تأسیساتی، اعم از چیلرهای تراکمی، هواساز، فن کویل، داکت اسپیلت، VRF و کولرهای گازی است. کارخانه این شرکت با تکیه بر دانش فنی روز اروپا، متخصصین کار آزموده و همچنین ماشین آلات و تجهیزات پیشرفته تمام اتوماتیک، محصولات با کیفیت و همگام با بالاترین استانداردهای تضمین کیفیت در اروپا را تولید می نماید. محصولات گرین در دو گروه تهویه مطبوع خانگی و صنعتی، مناسب برای انواع پروژه های مسکونی، اداری، تجاری، صنعتی، هتل ها، بیمارستان ها، مراکز خرید و اماکن عمومی است.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="aboutus-box">
          {/* must set background image */}
          <div className="container ">
            <div className="row">
              <div className="col-md-6 col-12">
                <div className="aboutus-data">
                  <div className="aboutus-data-h-box">
                    <h3 className="aboutus-data-h">گذشته شرکت بین المللی گرین
</h3>
                    <hr className="flex-fill hr-first" />
                  </div>
                  <div className="aboutus-data-p-box">
                    <p className="aboutus-data-p">
                    کمپانی تهویه مطبوع گرین تولیدکننده دستگاه‌های مرکزی و مستقل تأسیساتی، اعم از چیلرهای تراکمی، هواساز، فن کویل، داکت اسپیلت، VRF و کولرهای گازی است. کارخانه این شرکت با تکیه بر دانش فنی روز اروپا، متخصصین کار آزموده و همچنین ماشین آلات و تجهیزات پیشرفته تمام اتوماتیک، محصولات با کیفیت و همگام با بالاترین استانداردهای تضمین کیفیت در اروپا را تولید می نماید. محصولات گرین در دو گروه تهویه مطبوع خانگی و صنعتی، مناسب برای انواع پروژه های مسکونی، اداری، تجاری، صنعتی، هتل ها، بیمارستان ها، مراکز خرید و اماکن عمومی است.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="aboutus-2-box">
          {/* must set background image */}
          <div className="container ">
            <div className="row justify-content-end ">
              <div className="col-md-6 col-12">
                <div className="aboutus-2-data">
                  <div className="aboutus-2-data-h-box">
                    <h3 className="aboutus-2-data-h">پشم شیشه پارس
</h3>
                    <hr className="flex-fill hr-first" />
                  </div>
                  <div className="aboutus-2-data-p-box">
                    <p className="aboutus-2-data-p">
                    شرکت پشم شیشه پارس (سهامی خاص) در سال ۱۳۷۶ تاسیس و در سال ۱۳۸۰ به بهره برداری رسید. این واحد تولیدی علاوه بر توسعه و گسترش كمی و كیفی بازار عایق های حرارتی، با بهره گیری از تجارب و دانش مدیران شركت در سال ۱۳۸۶ فعالیت خود را در زمینه واردات تجهیزات سرمایشی و گرمایشی آغاز نمود. این شرکت هم اکنون با بیش از ۲۰ سال سابقه در ارائه خدمات مهندسی سیستم های تهویه مطبوع و تأسیسات ساختمانی، افتخار دارد تا به عنوان نماینده انحصاری تهویه مطبوع گرین، با هدف کاهش مصرف انرژی و حفظ محیط زیست، محصولات باکیفیت روز دنیا را ارائه داده و در خدمت جامعه مهندسین و متخصصین ایران باشد. تیم فنی و مهندسی تهویه مطبوع گرین علاوه بر فروش محصولات، در زمینه مشاوره، طراحی، نظارت، نصب، راه اندازی و خدمات پس از فروش نیز در جهت تامین آسایش و رضایت هرچه بیشتر مشتریان گام بر می دارد.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="Heads">
          <div className="container ">
            <div className="row">
              <div className="col-5">
                <hr className="hr-first" />
              </div>
              <h4 className="Heads-h col-2">{t("senior-managers")}</h4>
              <div className="col-5">
                <hr className="hr-first" />
              </div>
              <div className="col-md-4 col-12">
                <div className="Card">
                  <div className="Card-item">
                    <Image
                      src={pic1}
                      alt="Sunset in the mountains"
                      className="Card-img"
                    />
                    <div className="Card-data">
                      <p className="Card-title">{t("managers-name")}</p>
                      <p className="Card-p">{t("manager-position")}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-12">
                <div className="Card">
                  <div className="Card-item">
                    <Image
                      src={pic1}
                      alt="Sunset in the mountains"
                      className="Card-img"
                    />
                    <div className="Card-data">
                      <p className="Card-title">{t("managers-name")}</p>
                      <p className="Card-p">{t("manager-position")}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-12">
                <div className="Card">
                  <div className="Card-item">
                    <Image
                      src={pic1}
                      alt="Sunset in the mountains"
                      className="Card-img"
                    />
                    <div className="Card-data">
                      <p className="Card-title">{t("managers-name")}</p>
                      <p className="Card-p">{t("manager-position")}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
export default About_us;
