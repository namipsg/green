import "bootstrap/dist/css/bootstrap.css";
import "./globals.css";
import localFont from "next/font/local";
import Footer from "@/Components/Footer";
import Config from "../../config/default";
import { I18nProvider } from "@/i18n/i18n-context";
import { detectLanguage } from "@/i18n/server";
import Navbar from "@/Components/Nav/NavBar/Navbar";
import { dir } from "i18next";
import MuiProvider from "@/helpers/MuiProvider";

export const metadata = {
  title: "GREEN",
  description: "GREEN Compony",
};

// const yekan = localFont({
//   src: "../../public/Fonts/IRANSansX-Regular.woff",
// });

const getHomeData = async () => {
  try {
    const response = await fetch(Config.API.HOME.GET, {
      cache: "no-store",
    });
    return await response.json();
  } catch (error) {
    console.log("Error on Home SSR", error);
  }
};

export default async function RootLayout({ children }) {
  const homeData = await getHomeData();
  const { data } = homeData;
  const { header, footer, products } = data;
  const lng = await detectLanguage();

  return (
    <I18nProvider language={lng}>
      <html lang={lng} dir={dir(lng)}>
        <body className={` overflow-x-hidden`}>
          <MuiProvider rtl={dir(lng) === 'rtl'} >
            <Navbar menuData={header} />
            {children}
            <Footer menuData={footer} products={products} />
          </MuiProvider>
        </body>
      </html>
    </I18nProvider>
  );
}
