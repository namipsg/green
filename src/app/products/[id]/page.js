import Config, { PANELURL } from "../../../../config/default";
import ProductPage from "./productsContent";

const getProductData = async (id) => {
  try {
    const response = await fetch(Config.API.PRODUCT.GET + `?id=${id}`, {
      cache: "no-store",
    });

    return await response.json();
  } catch (error) {
    console.log("Error on product SSR", id, error);
  }
};

async function Product({ params }) {
  const { id } = params;

  const productData = await getProductData(id);

  const { data } = productData;

  return <ProductPage data={data} />;
}

export default Product;
