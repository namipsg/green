"use client";
import * as React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import "./index.css";
import Image from "next/image";
import phone from "../../../../public/img/phone.png";
import garatnie from "../../../../public/img/garantie.png";
import desk from "../../../../public/img/desk.png";
import comp from "../../../../public/img/comp.png";
import { PANELURL } from "../../../../config/default";
import { useTranslation } from "react-i18next";
import GetAppIcon from "@mui/icons-material/GetApp";
import {
  EmailIcon,
  FacebookMessengerIcon,
  TelegramIcon,
  TwitterIcon,
  WhatsappIcon,
} from "react-share";
import { IconButton } from "@mui/material";

const ProductPage = ({ data }) => {
  const { t } = useTranslation("products");
  
  return (
    <div>
      <div className="Video-sec">
        <video autoPlay loop muted className="video">
          <source
            src={PANELURL + data?.videoLink}
            type="video/mp4"
            className="video-1"
          />
        </video>
      </div>
      <section className="pad-50">
        <div className="Product-first-data-box">
          <div className="container">
            <div className="row">
              <div className="col-12 d-flex gap-3 mb-4">
                <hr className="flex-fill" />
                <h4 className="product-title-h font-800">{data?.name}</h4>
                <hr className="flex-fill" />
              </div>
              <p className="product-dec text-center mb-5">{data?.description}</p>
              <div className="col-md-6 col-lg-3">
                <div className="product-card-box">
                  <div className="product-card-img">
                    <Image src={phone} alt="phone" />
                  </div>
                  <div className="product-card-data">
                    <p>{t("professional-consultation")}</p>
                    {data?.section1}
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-lg-3">
                <div className="product-card-box">
                  <div className="product-card-img">
                    <Image src={desk} alt="phone" />
                  </div>
                  <div className="product-card-data">
                    <p>{t("active-representatives")}</p>
                    {data?.section2}
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-lg-3">
                <div className="product-card-box">
                  <div className="product-card-img">
                    <Image src={comp} alt="phone" />
                  </div>
                  <div className="product-card-data">
                    <p>{t("warranty-support")}</p>
                    {data?.section3}
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-lg-3">
                <div className="product-card-box">
                  <div className="product-card-img">
                    <Image src={garatnie} alt="phone" />
                  </div>
                  <div className="product-card-data">
                    <p>{t("pieces-warranty")}</p>
                    {data?.section4}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="product-seris">
          <div className="container">
            <div className="row">
              <div className="col-12 d-flex gap-3 mb-4">
                <hr className="flex-fill" />
                <h4 className="product-seris-h">{t("product-series")}</h4>

                <hr className="flex-fill" />
              </div>

              {data?.series.map((item, index) => (
                <div key={"serie_" + index} className="col-md-4">
                  <div className="Card-box ">
                    <div className="card-box-img">
                      <img
                        src={PANELURL + item.image}
                        alt="pic"
                        className="card-pic"
                      />
                    </div>
                    <div className="card-box-title">
                      <h5>{item.name}</h5>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="Catalog">
          <div className="container">
            <div className="row ">
              <div className="col-12 d-flex gap-3 mb-4">
                <hr className="flex-fill" />
                <h4>{t("download-catalog")}</h4>
                <hr className="flex-fill" />
              </div>
              <div className="col-12 d-flex justify-content-center">
                <a href={PANELURL + data?.catalog}>
                  <button className="btn btn-lg btn-success">
                    <GetAppIcon /> {t("download-catalog")}
                  </button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="Accardion"> 
          <div className="container">
            <div className="row">
              <div className="col-12 d-flex gap-3 mb-4">
                <hr className="flex-fill" />
                <h4>{t("frequently-asked-questions")}</h4>
                <hr className="flex-fill" />
              </div>
              <div className="col-12">
                {data?.faqs?.map((item, index) => (
                  <Accordion key={"acc_" + index}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1-content"
                      id="panel1-header"
                    >
                      {item.question}
                    </AccordionSummary>
                    <AccordionDetails  dangerouslySetInnerHTML={{ __html: item.answer }}></AccordionDetails>
                  </Accordion>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="container d-flex gap-3">
          <IconButton
            href={`sms://body=Check out this site ${window.location.href}`}
            title="I wanted you to see this site"
          >
            <FacebookMessengerIcon size={32} round />
          </IconButton>
          <IconButton
            href={`http://twitter.com/share?text=Check out this site&url=${window.location.href}`}
          >
            <TwitterIcon size={32} round />
          </IconButton>
          <IconButton
            href={`https://telegram.me/share/url?url=${window.location.href}&text=Check out this site`}
          >
            <TelegramIcon size={32} round />
          </IconButton>
          <IconButton
            href={`whatsapp://send?text=Check out this site ${window.location.href}`}
            data-action="share/whatsapp/share"
          >
            <WhatsappIcon size={32} round />
          </IconButton>
          <IconButton
            href={`mailto:?subject=I wanted you to see this site&body=Check out this site ${window.location.href}`}
          >
            <EmailIcon size={32} round />
          </IconButton>
        </div>
      </section>
    </div>
  );
};

export default ProductPage;
