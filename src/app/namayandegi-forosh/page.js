import Config from "../../../config/default";
import NamayandegiForoshContent from "@/Components/namayandegiForoshContent/namayandegiForoshContent";

const getStates = async () => {
  const states = await fetch(Config.API.LOCATION.STATE, {
    cache: "no-store",
  });
  return await states.json();
};

const Namayandegi = async () => {
  const states = await getStates();

  return (
    <>
      <NamayandegiForoshContent
        states={states?.data}
        type={1}
        label={"نمایندگی فروش"}
      />
    </>
  );
};

export default Namayandegi;
