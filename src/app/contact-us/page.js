"use client";
import TopNav from "@/Components/Nav/TopNav";
import "./index.css";
import Image from "next/image";
import phone from "../../../public/img/icons8-hang-up-50.png";
import fax from "../../../public/img/icons8-fax-50.png";
import mail from "../../../public/img/icons8-email-50.png";
import { Email, Fax, Phone } from "@mui/icons-material";
import Instagram from "../../../public/img/icons8-instagram-50.png";
import facebook from "../../../public/img/icons8-facebook-50.png";
import twitter from "../../../public/img/icons8-twitter-squared-50.png";
import youtube from "../../../public/img/icons8-youtube-50.png";
import linkedin from "../../../public/img/icons8-linkedin-50.png";
import pinterest from "../../../public/img/icons8-pinterest-50.png";
import pic from "../../../public/img/banner-footer.jpg";
import Footer from "@/Components/Footer";
import Link from "next/link";
import Layout from "@/layout";
import { Button } from "@/Components/elements/Button";
import ContactForm from "@/Components/contactForm";
import { useTranslation } from "react-i18next";

function Contact_us() {
  const { t } = useTranslation("contact-us");
  return (
    <div id="contact-us">
      <section>
        <div className="top-pic">
          <div className="container ">
            <div className="row">
              <div className="col-12">
                <div className="title-box">
                  <h2 className="title-h font-800">{t("contact-us")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="contact-data">
          <div className="container ">
            <div className="row">
              <div className="col-12 col-md-4">
                <div className="contact-data-box">
                  <div className="contact-box-title-div">
                    <h4 className="contact-box-h font-800">
                      {t("head-office")}
                    </h4>
                    <hr className="flex-fill hr" />
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Phone />
                    </div>
                    <span>{t("contact-number")}: 02141749</span>
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Fax />
                    </div>
                    <span>{t("email")} : info@greenac.co</span>
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Email />
                    </div>
                    <span>{t("fax")} : info@green.co</span>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-4">
                <div className="contact-data-box">
                  <div className="contact-box-title-div">
                    <h4 className="contact-box-h font-800">
                      {/* {t("head-office")} */}
                      نمایشگاه مرکزی
                    </h4>
                    <hr className="flex-fill hr" />
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Phone />
                    </div>
                    <span>{t("contact-number")}: 02141749</span>
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Fax />
                    </div>
                    <span>{t("email")} : info@greenac.co</span>
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Email />
                    </div>
                    <span>{t("fax")} : info@green.co</span>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-4">
                <div className="contact-data-box">
                  <div className="contact-box-title-div">
                    <h4 className="contact-box-h font-800">
                      {/* {t("head-office")} */}
                      کارخانه
                    </h4>
                    <hr className="flex-fill hr" />
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Phone />
                    </div>
                    <span>{t("contact-number")}: 02141749</span>
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Fax />
                    </div>
                    <span>{t("email")} : info@greenac.co</span>
                  </div>
                  <div className="contact-box-line-div">
                    <div className="contact-box-logo">
                      <Email />
                    </div>
                    <span>{t("fax")} : info@green.co</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="social-media">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="social-media-box">
                  <div className="titr-div col-12">
                    <hr className="flex-fill hr" />
                    <h4 className="social-media-h font-800">
                      {t("social-media")}
                    </h4>
                    <hr className="flex-fill hr" />
                  </div>
                  <div className="social-media-box-div">
                    <Link href="#">
                      <Image
                        src={facebook}
                        alt=""
                        className="social-media-box-icon"
                        height={30}
                      />
                    </Link>
                    <Link href="#">
                      <Image
                        src={Instagram}
                        alt=""
                        className="social-media-box-icon"
                        height={30}
                      />
                    </Link>
                    <Link href="#">
                      <Image
                        src={linkedin}
                        alt=""
                        className="social-media-box-icon"
                        height={30}
                      />
                    </Link>
                    <Link href="#">
                      <Image
                        src={twitter}
                        alt=""
                        className="social-media-box-icon"
                        height={30}
                      />
                    </Link>
                    <Link href="#">
                      <Image
                        src={pinterest}
                        alt=""
                        className="social-media-box-icon"
                        height={30}
                      />
                    </Link>
                    <Link href="#">
                      <Image
                        src={youtube}
                        alt=""
                        className="social-media-box-icon"
                        height={30}
                      />
                    </Link>
                  </div>
                </div>
                <div className="social-media-pic-box">
                  <img
                    src={"/img/headers/contact-us-2.jpg"}
                    alt="pic"
                    className="social-media-pic-box-pic"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="map">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="map-box">
                  <div className="map-box-h col-12">
                    <hr className="flex-fill hr" />
                    <h4 className="map-box-h-title">
                      {t("head-office-address-title")}
                    </h4>
                    <hr className="flex-fill hr" />
                  </div>
                  <div className="map-box-title">
                    <p>{t("head-office-address")}</p>
                  </div>
                  <div className="map-box-frame">
                    <iframe
                      className="map-box-frame-map"
                      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5371.969279278773!2d51.422289695418804!3d35.70727720404329!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e01d85db406c9%3A0x65cd2af68b6d6c75!2z2YbZhdin24zYtNqv2KfZhyDYr9in2KbZhduMINiq2YfZiNuM2Ycg2YXYt9io2YjYuSDar9ix24zZhg!5e0!3m2!1sen!2s!4v1705734295691!5m2!1sen!2s"
                      loading="lazy"
                      referrerpolicy="no-referrer-when-downgrade"
                    ></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="map">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="map-box">
                  <div className="map-box-h col-12">
                    <hr className="flex-fill hr" />
                    <h4 className="map-box-h-title">
                      {t("head-office-address-title")}
                    </h4>
                    <hr className="flex-fill hr" />
                  </div>
                  <div className="map-box-title">
                    <p>{t("head-office-address")}</p>
                  </div>
                  <div className="map-box-frame">
                    <iframe
                      className="map-box-frame-map"
                      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5371.969279278773!2d51.422289695418804!3d35.70727720404329!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e01d85db406c9%3A0x65cd2af68b6d6c75!2z2YbZhdin24zYtNqv2KfZhyDYr9in2KbZhduMINiq2YfZiNuM2Ycg2YXYt9io2YjYuSDar9ix24zZhg!5e0!3m2!1sen!2s!4v1705734295691!5m2!1sen!2s"
                      loading="lazy"
                      referrerpolicy="no-referrer-when-downgrade"
                    ></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="map">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="map-box">
                  <div className="map-box-h col-12">
                    <hr className="flex-fill hr" />
                    <h4 className="map-box-h-title">
                      {t("head-office-address-title")}
                    </h4>
                    <hr className="flex-fill hr" />
                  </div>
                  <div className="map-box-title">
                    <p>{t("head-office-address")}</p>
                  </div>
                  <div className="map-box-frame">
                    <iframe
                      className="map-box-frame-map"
                      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5371.969279278773!2d51.422289695418804!3d35.70727720404329!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e01d85db406c9%3A0x65cd2af68b6d6c75!2z2YbZhdin24zYtNqv2KfZhyDYr9in2KbZhduMINiq2YfZiNuM2Ycg2YXYt9io2YjYuSDar9ix24zZhg!5e0!3m2!1sen!2s!4v1705734295691!5m2!1sen!2s"
                      loading="lazy"
                      referrerpolicy="no-referrer-when-downgrade"
                    ></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="pad-50">
        <div className="form">
          <div className="container">
            <div className="contact-form-title col-12">
              <hr className="flex-fill hr" />
              <h4 className="map-box-h-title">{t("contact-form")}</h4>
              <hr className="flex-fill hr" />
            </div>
            <div className="col-12">
              <div className="contact-form-des-box">
                <p className="contact-form-des">{t("description")}</p>
              </div>
            </div>
            <ContactForm />
          </div>
        </div>
      </section>
    </div>
  );
}
export default Contact_us;
