import "../[id]/index.css";
import Config from "../../../../config/default";
import ViewNews from "@/Components/News/viewNews";

const getNewsData = async (id) => {
  try {
    const response = await fetch(Config.API.NEWS.GET + `?id=${id}`, {
      cache: "no-store",
    });

    return await response.json();
  } catch (error) {
    console.log("Error on news SSR", id, error);
  }
};

async function NewPage({ params }) {
  const { id } = params;

  const newsData = await getNewsData(id);

  const { data } = newsData;
  return <ViewNews data={data} />;
}
export default NewPage;
