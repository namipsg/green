import NewsPageContent from "../../Components/News/newsPageContent";
import Config from "../../../config/default";

const getCategoryData = async () => {
  const categoryData = await fetch(Config.API.CATEGORY.GETALL, {
    cache: "no-store",
  });
  return await categoryData.json();
};

const getNewsList = async () => {
  const newsList = await fetch(Config.API.NEWS.GETALL, {
    cache: "no-store",
  });
  return await newsList.json();
};

const NewsPage = async () => {
  const categoryData = await getCategoryData();
  const newsList = await getNewsList();
  const newsData = await getNewsList();

  return (
    <NewsPageContent
      newsData={newsData}
      newsList={newsList}
      categoryData={categoryData}
    />
  );
};
export default NewsPage;
