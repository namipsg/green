import UnitConverter from "@/Components/UnitConverter";
import React from "react";
import './index.css'
import Config from "../../../config/default";

const getUnits = async () => {
  const units = await fetch(Config.API.UNIT.GET);
  return await units.json();
};

const Page = async () => {
  const unitTypes = await getUnits();
  return <UnitConverter units={unitTypes} />;
};

export default Page;
