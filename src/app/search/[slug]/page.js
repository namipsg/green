import SearchPage from "@/Components/Search";
import Config, { PANELURL } from "../../../../config/default";

const getSearchData = async (slug) => {
  try {
    const response = await fetch(Config.API.SEARCH.GET + `?text=${slug}`, {
      cache: "no-store",
    });

    return await response.json();
  } catch (error) {
    console.log("Error on product SSR", id, error);
  }
};

async function Page({ params }) {
  const { slug } = params;

  const searchData = await getSearchData(slug);

  const { data } = searchData;

  return <SearchPage searchData={data} />;
}

export default Page;
