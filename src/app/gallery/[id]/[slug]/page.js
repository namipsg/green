import GalleryItem from "@/Components/Gallery/galleryItem/galleryItem";
import React from "react";
import Config from "../../../../../config/default";

const getGalleryItems = async (parentId) => {
  const response = await fetch(
    Config.API.GALLERY.GET + `?parentId=${parentId}`,
    {
      cache: "no-store",
    }
  );

  if (response.status === 200) {
    return await response.json();
  }
};

const Page = async ({ params }) => {
  const { slug } = params;
  if (!slug) {
    return <>Not found</>;
  }

  const data = await getGalleryItems(slug);

  return <GalleryItem finalData={data} />;
};

export default Page;
