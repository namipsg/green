import GalleryGroup from "@/Components/Gallery/galleryGroup/galleryGroup";
import Config from "../../../config/default";

const getGalleryGroupData = async () => {
  const galleryGroupData = await fetch(Config.API.GALLERY.GROUP, {
    cache: "no-store",
  });
  return await galleryGroupData.json();
};

const Page = async () => {
  const groupsData = await getGalleryGroupData();
  return <GalleryGroup groupsData={groupsData} />;
};

export default Page;
