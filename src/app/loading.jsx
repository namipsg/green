import Image from "next/image";
import React from "react";

const Loading = () => {
  return (
    <div className="loading-container">
      <Image
        src={"/spinning-circles.svg"}
        width={72}
        height={72}
        alt="در حال بارگذاری..."
      />
    </div>
  );
};

export default Loading;
