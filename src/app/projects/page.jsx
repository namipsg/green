import ProjectsContent from "@/Components/projectContent/page";
import Config from "../../../config/default";

const getProjectList = async () => {
  const projectList = await fetch(Config.API.PROJECT.GETALL, {
    cache: "no-store",
  });
  return await projectList.json();
};

const Projects = async () => {
  const projectList = await getProjectList();
  
  return <ProjectsContent finalData={projectList} />;
};
export default Projects;
