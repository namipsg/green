import React from "react";
import "./index.css";
import InstallationRequest from "@/Components/installationRequest";
import Config from "../../../config/default";

const getStates = async () => {
  const states = await fetch(Config.API.LOCATION.STATE, {
    cache: "no-store",
  });
  return await states.json();
};

const Page = async () => {
  const states = await getStates();
  return <InstallationRequest states={states?.data} />;
};

export default Page;
