"use client";
import * as React from "react";
import { prefixer } from "stylis";
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider, ThemeProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import { createTheme } from "@mui/material/styles";

const RtlTheme = createTheme({
  direction: "rtl",
  typography: {
    fontFamily: "YekanBakh",
  },
  palette: {
    primary: {
      main: "#198754",
      light: "#4caf50",
      dark: "#1b5e20",
    },
  },
});

const LtrTheme = createTheme({
  direction: "ltr",
});

const rtlCache = createCache({
  key: "muirtl",
  stylisPlugins: [prefixer, rtlPlugin],
});

const ltrCache = createCache({
  key: "mui",
});

export default function MuiProvider({ children, rtl }) {
  return (
    <CacheProvider value={rtl ? rtlCache : ltrCache}>
      <ThemeProvider theme={rtl ? RtlTheme : LtrTheme}>
        {children}
      </ThemeProvider>
    </CacheProvider>
  );
}
