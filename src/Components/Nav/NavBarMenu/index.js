"use client";
import "./index.css";
import React from "react";
import NavBarItem from "../NavBarItem/index.js";
import Link from "next/link";

const NavMenu = ({ menuData }) => {
  const renderItems = menuData?.[0];
  return (
    <nav className="NavBarr">
      <ul className="NavBar-ul ">
        {renderItems?.map((item) => {
          if (item?.childs && item.childs.length > 0) {
            return (
              <NavBarItem title={item.name} key={item.id} items={item.childs} />
            );
          } else {
            return (
              <Link className="navbar-li-a" href={item.url} key={item.id}>
                {item.name}
              </Link>
            );
          }
        })}
      </ul>
    </nav>
  );
};

export default NavMenu;
