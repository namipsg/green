"use client";
import Link from "next/link";
import "./index.css";
import React, { useState } from "react";
import { ExpandMoreOutlined } from "@mui/icons-material";

const NavBarItem = ({ title, items }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="dropdown">
      <button className="btn btn-navbar-list" onClick={toggleDropdown}>
        {title}
        <ExpandMoreOutlined />
      </button>
      {isOpen && (
        <div className="dropdown-content">
          <div className="container ">
            <div className=" dropdown-content-navbar">
              <ul className="dropdown-content-ul flex flex-column gap-y-2 flex-wrap">
                {items?.map((item) => (
                  <Link href={item.url} key={item.id}>
                    <li className="dropdown-content-li">{item.name}</li>
                  </Link>
                ))}
              </ul>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default NavBarItem;
