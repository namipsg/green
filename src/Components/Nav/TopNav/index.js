"use client";
import "./index.css";
import Instagram from "../../../../public/img/icons8-instagram-50.png";
import facebook from "../../../../public/img/icons8-facebook-50.png";
import twitter from "../../../../public/img/icons8-twitter-squared-50.png";
import youtube from "../../../../public/img/icons8-youtube-50.png";
import linkedin from "../../../../public/img/icons8-linkedin-50.png";
import pinterest from "../../../../public/img/icons8-pinterest-50.png";
import NavBar from "../NavBar/index.js";
import Image from "next/image";
import Link from "next/link";
import { SnackbarProvider } from "notistack";

function TopNav({ menuData }) {
  return (
    <>
      <SnackbarProvider />
      <section className="nav-top">
        <div className="container">
          <div className="row">
            <div className="col-6 co-md-7 col-lg-6">
              <Link href="/">
                <img height={64} src={'/img/logo1.png'} alt="logo" className="logo" />
              </Link>
            </div>
            {/* <div className="col-3 col-md-3 col-lg-3  ">
              <LangDropDown />
            </div> */}
            <div className="col-3 col-md-3 col-lg-3 flex-fill">
              <div className="Media-Top d-flex gap-2 justify-content-end">
                <Link href="#">
                  <Image src={facebook} alt="" className="media" height={30} />
                </Link>
                <Link href="#">
                  <Image src={Instagram} alt="" className="media" height={30} />
                </Link>
                <Link href="#">
                  <Image src={linkedin} alt="" className="media" height={30} />
                </Link>
                <Link href="#">
                  <Image src={twitter} alt="" className="media" height={30} />
                </Link>
                <Link href="#">
                  <Image src={pinterest} alt="" className="media" height={30} />
                </Link>
                <Link href="#">
                  <Image src={youtube} alt="" className="media" height={30} />
                </Link>
              </div>
            </div>
          </div>
        </div>
        <NavBar menuData={menuData} />
      </section>
    </>
  );
}
export default TopNav;
