"use client";
import { ChevronLeft } from '@mui/icons-material';
import { Box, Collapse } from '@mui/material';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import React, { useState } from 'react';
import { useTranslation } from "react-i18next";

const NavDropDown = ({ item, closeParent }) => {
    const [isOpen, setOpen] = useState(false);
    const router = useRouter();

    return <li className="sub-menu" >
        <a className='d-flex justify-content-between align-items-center' onClick={() => setOpen(!isOpen)}>
            {item.name}
            <ChevronLeft className='mr-auto d-block cursor-pointer' style={{
                transform: isOpen ? 'rotate(90deg)' : 'rotate(270deg)',
                transition: 'transform ease .4s'
            }} />
        </a>
        <Collapse in={isOpen}>
            <Box component={'ul'}>
                {item.childs.map(((child) => <div
                    onClick={() => {
                        setOpen(false);
                        closeParent(false);
                        router.push(child.url)
                    }}
                    className='cursor-pointer'
                    key={item.id + '_child_' + child.id}

                >
                    {child.name}
                </div>))}
            </Box>
        </Collapse>
    </li>
}

const Navside = ({ sideOpen, setSideOpen, menuData }) => {
    const { t } = useTranslation("navbar");
    return (
        <div id="mySidenav" className="sidenav" style={{ width: sideOpen ? 320 : 0 }}>
            <a className="closebtn cursor-pointer" onClick={() => setSideOpen(false)}>
                ×
            </a>
            <div className="sidemenu" style={{
                minWidth: 'fit-content'
            }}>
                <nav className="animated bounceInDown">
                    <ul>
                        <li>
                            <Link href={'/'} onClick={() => setSideOpen(false)}>
                                صفحه اصلی
                            </Link>
                        </li>
                        {menuData?.map(item => {
                            if (item.childs && item.childs.length > 0) {
                                return <NavDropDown item={item} key={'parent_' + item.id} closeParent={setSideOpen} />

                            } else {
                                return <li key={'link_' + item.id}>
                                    <Link href={item.url} onClick={() => setSideOpen(false)}>
                                        {item.name}
                                    </Link>
                                </li>
                            }
                        })}
                    </ul>
                </nav>
            </div>
        </div>

    );
}

export default Navside;