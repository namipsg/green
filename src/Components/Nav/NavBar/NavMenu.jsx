"use client";
import React, { useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { getCookie } from "cookies-next";
import i18next from "i18next";
import Link from "next/link";
import Language from "./language";
import _ from 'lodash'
import getNavDescription from "@/helpers/getNavDescription";
import { SnackbarProvider, enqueueSnackbar } from "notistack";
import { useRouter } from "next/navigation";

const locales = [
  {
    key: "fa",
    value: "فارسی",
    rtl: true,
    flag: '/img/iranf.png'
  },
  {
    key: "en",
    value: "English",
    rtl: false,
    flag: '/img/pngegg.png'
  },
];

const NavMenu = ({ setSideOpen, menuData }) => {
  const router = useRouter();
  const [activeLanguage, setActiveLanguage] = useState(
    i18next.language
  );
  const [search, setSearch] = useState('');

  const activeLangData = () => locales.find(x => x.key === activeLanguage);

  function change(l) {
    setActiveLanguage(l);
    i18next.changeLanguage(l);
    window.location.reload();

  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (search.length < 3) {
      return enqueueSnackbar('عبارت جستجو حداقل باید ۳ حرفی باشد', {
        variant: 'warning'
      })
    }
    router.push(`/search/${encodeURIComponent(search)}`)

  }


  const { t } = useTranslation("navbar");
  return (
    <section className="full-top">
      <SnackbarProvider anchorOrigin={{
        horizontal: 'center',
        vertical: 'top'
      }} />
      <section className="top-menu">
        <div className="container">
          <div className="d-flex justify-content-between align-items-center">
            <div className="col-3 col-lg-7 col-md-7">
              <div className="logo-top-div">
                <a href="/">
                  <img src="/img/logo1.png" className="logo-top" />
                </a>
              </div>
            </div>
            {/* <div className="col-3 col-lg-3 col-md-3 d-flex justify-content-end align-items-center align-items-sm-center justify-content-xl-end align-items-xl-center">
              <div className="text-start mt65">
                <div
                  className="language-dropdown cursor-pointer"
                  style={{
                    position: "relative",
                  }}
                >
                  <span>
                    <img src={activeLangData().flag} className="brd" width={40} />
                    {activeLangData().value}
                  </span>
                  <i className="arrow down" />
                  <div className="language-dropdown-content" onClick={() => i18next}>
                    {_.cloneDeep(locales).filter(x => x.key !== activeLanguage).map((item) => (
                      <div key={item.key} onClick={() => change(item.key)} className="w-100">
                        <span className="cursor-pointer">
                          <img src={item.flag} />
                          {item.value}
                        </span>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div> */}

            <div className="col-6 col-lg-2 col-md-2 d-flex justify-content-end align-items-center align-items-sm-center justify-content-xl-end align-items-xl-center mr-auto">
              <div
                className="d-sm-flex align-items-sm-center"
                style={{ marginTop: 8 }}
              >
                <div className="social-div">
                  <a href="#">
                    <img
                      src="/img/icons8-facebook-50.png"
                      className="social-logo"
                    />{" "}
                  </a>
                  <a href="#">
                    <img
                      src="/img/icons8-instagram-50.png"
                      className="social-logo"
                    />{" "}
                  </a>
                  <a href="#">
                    <img
                      src="/img/icons8-linkedin-50.png"
                      className="social-logo"
                    />
                  </a>
                  <a href="#">
                    <img
                      src="/img/icons8-pinterest-50.png"
                      className="social-logo"
                    />
                  </a>
                  <a href="#">
                    <img
                      src="/img/icons8-twitter-squared-50.png"
                      className="social-logo"
                    />{" "}
                  </a>
                  <a href="#">
                    <img
                      src="/img/icons8-youtube-50.png"
                      className="social-logo"
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="menu-sec">
        <div className="container container-menu">
          <div className="row">
            <div className="col-xl-9 col-md-6 col-2 col-2">
              <div className="d-xl-flex justify-content-xl-start">
                <nav className="navbar navbar-light navbar-expand-xl">
                  <div className="container-fluid">
                    <span
                      style={{ fontSize: 30, cursor: "pointer", zIndex: 99 }}
                      onClick={() => setSideOpen(true)}
                    >
                      <span className="navbar-toggler-icon" />
                    </span>
                    <div className="collapse navbar-collapse" id="navcol-3">
                      <ul className="navbar-nav">
                        {menuData?.map(item => {
                          if (item.childs && item.childs.length > 0) {
                            const firstChunk = item.childs?.slice(0, 4);
                            const secondChunk = item.childs?.slice(4);
                            return <li key={'parent_' + item.id} className="nav-item dropdown dropdown-hover ">
                              <Link
                                className="nav-link dropdown-toggle"
                                href={item.url}
                                id="navbarDropdown"
                                role="button"
                                data-mdb-toggle="dropdown"
                                aria-expanded="false"
                              >
                                {item.name}
                              </Link>
                              {/* Dropdown menu */}
                              <div
                                className="dropdown-menu w-100 mt-0"
                                aria-labelledby="navbarDropdown"
                                style={{
                                  borderTopLeftRadius: 0,
                                  borderTopRightRadius: 0,
                                }}
                              >
                                <div className="container">
                                  <div className="row mt-5 mb-3">
                                    <div className="col-md-3 col-lg-3 mb-3 mb-lg-0 d-flex">
                                      <div className="list-group list-group-flush">
                                        {firstChunk.map(((child) => <a
                                          key={item.id + '_child_' + child.id}
                                          href={child.url}
                                          className="list-group-item list-group-item-action"
                                        >
                                          {child.name}
                                        </a>))}

                                      </div>
                                      {
                                        secondChunk.length > 0 && <div className="list-group list-group-flush">
                                          {secondChunk.map(((child) => <a
                                            key={item.id + '_child_' + child.id}
                                            href={child.url}
                                            className="list-group-item list-group-item-action"
                                          >
                                            {child.name}
                                          </a>))}

                                        </div>
                                      }
                                    </div>

                                    <div className="col-md-7 col-lg-7 mb-3 mb-lg-0">
                                      <div className="list-group list-group-flush">
                                        <h3>{item.name}</h3>
                                        <p>{item.description || getNavDescription(item.name)}</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                          } else {
                            return <li key={'link_' + item.id} className="nav-item">
                              <Link className="nav-link text-nowrap" href={item.url}>
                                {item.name}
                              </Link>
                            </li>
                          }
                        })}

                      </ul>
                    </div>
                  </div>
                </nav>
              </div>
            </div>
            <div className="col-xl-3 col-md-6 col-10 d-flex justify-content-end align-items-center">
              <form onSubmit={handleSubmit} className="d-flex gap-1" style={{
                minWidth: 'fit-content'
              }}>
                <input
                  type="text"
                  className="search"
                  placeholder={t("product-content-search")}
                  value={search}
                  onChange={(e) => setSearch(e.target.value)}
                />
                <div onClick={handleSubmit}>
                  <svg
                    width="25.5"
                    height={32}
                    viewBox="0 0 32 32"
                    className="search-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    style={{ width: "25.5px", height: "auto" }}
                    fill="#ffffff"
                  >
                    <path
                      d="m27.766 26.734-6.657-6.609c1.5-1.734 2.344-3.938 2.344-6.375 0-5.344-4.406-9.75-9.75-9.75C8.313 4 4 8.406 4 13.75c0 5.39 4.36 9.75 9.703 9.75 2.39 0 4.594-.844 6.328-2.344l6.61 6.657c.187.14.375.187.609.187.188 0 .375-.047.516-.188.28-.28.28-.796 0-1.078zM13.75 22a8.232 8.232 0 0 1-8.25-8.25 8.232 8.232 0 0 1 8.25-8.25c4.547 0 8.25 3.703 8.25 8.25A8.232 8.232 0 0 1 13.75 22z"
                      fill="#ffffff"
                    />
                  </svg>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </section>
  );
};

export default NavMenu;
