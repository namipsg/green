import React from 'react';
import './style.css'

const Topnav = () => {
    return (<div id="overlay" style={{ opacity: 0, display: "none" }}>
        <div className="logop">
            <a href="/">
                <img src="/img/logo1.png" className="logo-top pds" />
            </a>
        </div>
        <div id="progstat">100%</div>
        <div id="progress" style={{ width: "100%" }} />
    </div>
    );
}

export default Topnav;