﻿"use client";
import "./index.css";
import { MenuOutlined, SearchOutlined } from "@mui/icons-material";
import NavMenu from "../NavBarMenu";
import { useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import BurgerDropDown from "@/Components/elements/BurgerDropDown";

function NavBar({ menuData }) {
  const { t } = useTranslation("navbar");
  const [searchIsOpen, setSearchIsOpen] = useState(false);
  const ref = useRef();
  
  const search = () => {
    setSearchIsOpen(!searchIsOpen);
    if (searchIsOpen) {
      setTimeout(() => {
        ref.current?.focus();
      });
    }
  };

  return (
    <nav className="NavBar">
      <div className="container ">
        <div className="d-flex">
          <div className="flex-fill">
            <div className="NavBar-list">
              <BurgerDropDown
                title={<MenuOutlined className="text-white " />}
              />
              <div id="NavMenu">
                <NavMenu menuData={menuData} />
              </div>
            </div>
          </div>
          <div className="d-flex flex-row-reverse align-items-center">
            <button className="btn btn-searchbar">
              <SearchOutlined />
            </button>
            <input
              ref={ref}
              className="open"
              placeholder={t("product-content-search")}
              type="text"
              name=""
              id="nav-search"
            />
          </div>
        </div>
      </div>
    </nav>
  );
}
export default NavBar;
