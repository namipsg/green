'use client';
import React, { useState } from 'react';
import Topnav from './TopNav';
import Navside from './Navside';
import NavMenu from './NavMenu';

const Navbar = ({ menuData }) => {
    const [sideOpen, setSideOpen] = useState(false)
    return (
        <>
            <Topnav />
            <Navside sideOpen={sideOpen} setSideOpen={setSideOpen} menuData={menuData} />
            <NavMenu setSideOpen={setSideOpen} menuData={menuData} />
        </>

    );
}

export default Navbar;