﻿"use client";
import "../LastProject/index.css";
import { PANELURL } from "../../../config/default";
import Link from "next/link";
import { useTranslation } from "react-i18next";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper/modules";
import "swiper/css";
import "swiper/css/pagination";

function LastProject({ finalData }) {
  const { t } = useTranslation("projects");
  const { data } = finalData;
  return (
    <div className="LastProject">
      <div className="container ">
        <div className="row">
          <h2 className="h2">{t("last-projects")}</h2>
          <p className="news-p p">{t("green-last-projects")}</p>
        </div>
        <Swiper
          dir="rtl"
          slidesPerView={3}
          spaceBetween={30}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
          pagination={{
            clickable: true,
          }}
          breakpoints={{
            250: {
              slidesPerView: 1,
              spaceBetween: 50,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 30,
            },                 
            1024: {
              slidesPerView: 3,
              spaceBetween: 50,
            },
          }}

          modules={[Autoplay, Pagination]}
          className="mt-5"
        >
          {data?.slice(-5).map((item) => (
            <SwiperSlide className="swiperslide" key={"slide_" + item.id}>
              <div className="slider-box">
                <div className="slider-box-pic">
                  <img
                    src={PANELURL + item.image}
                    className="img-fluid w-100 rounded"
                    alt={item.name}
                  />
                </div>
                <div className="slider-data-box mt-3 mb-5">
                  <div className="slider-data-top">
                    <h4 className="slider-data-title m-0 mt-3">{item.name}</h4>
                    <p className="slider-data-des m-0">{item.categoryName}</p>
                  </div>
                  <h5 className="slider-data-city">{item.state}</h5>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
        {/* <Swiper
          dir="rtl"
          centeredSlides={false}
          slidesPerView={1}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
          pagination={{
            clickable: true,
          }}
          spaceBetween={1}
          breakpoints={{
            768: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            1024: {
              slidesPerView: 3,
              spaceBetween: 50,
            },
          }}
          navigation={false}
          modules={[Autoplay]}
          className="mySwiper myswiper"
        >
          {data?.map((item) => (
            <SwiperSlide className="swiperslide" key={item.id}>
              <div className="slider-box w-100">
                <div className="slider-box-pic w-100">
                  <img
                    src={PANELURL + item.image}
                    className="img-fluid rounded"
                    alt={item.name}
                  />
                </div>
                <div className="slider-data-box">
                  <div className="slider-data-top">
                    <h4 className="slider-data-title m-0 mt-3">{item.name}</h4>
                    <p className="slider-data-des m-0">{item.categoryName}</p>
                  </div>
                  <h5 className="slider-data-city">{item.state}</h5>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper> */}
      </div>
    </div>
  );
}
export default LastProject;
