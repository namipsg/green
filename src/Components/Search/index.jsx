'use client'
import React from 'react';
import './index.css'
import _ from 'lodash'
import { BASEURL, PANELURL } from '../../../config/default';
import Link from 'next/link';

const labelsGuide = {
    News: 'اخبار',
    Product: 'محصولات',
    Project: 'پروژه ها'
}

const linksGuide = {
    News: '/news/',
    Product: '/products/',
    Project: '/projects'
}


const SearchPage = ({ searchData }) => {
    const groups = _.groupBy(searchData, 'section');
    if (searchData.length < 1) {
        return <div div className='container result-container mt-10' >
            <div class="alert alert-warning" role="alert">
                نتیجه ای متناسب با جتستجوی شما یافت نشد
            </div>
        </div >
    }
    return (
        <div className='container result-container mt-10'>
            <h5 className='mb-5 fs-3'>نتایج جستجو</h5>

            {Object.keys(groups).map((item, index) => (
                <div className='w-100' key={'group_' + index}>
                    <h5 className='text-secondary mt-3'>{labelsGuide[item]}</h5>
                    {groups[item].map((child, cIndex) =>
                        <Link href={item === 'Project' ? '#' : `${linksGuide[item]}${child.id}`} className="d-flex border-bottom px-4 py-3 gap-3" key={index + '_child_' + cIndex}>
                            <img className='img-fluid rounded' src={PANELURL + child.imageUrl} style={{
                                maxHeight: 210,
                                aspectRatio: '16 / 9',
                                objectFit: 'cover'
                            }} />
                            <p>{child.name || child.title}</p>
                        </Link>
                    )}

                </div>)
            )}
        </div>
    );
}

export default SearchPage;