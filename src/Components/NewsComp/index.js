
"use client";
import React from "react";
import "./index.css";
import Image from "next/image";
import { Button } from "../Button";
import { useTranslation } from "react-i18next";

const NewsComp = ({ NewsItems }) => {
  const { t } = useTranslation("news");
  return (
    <div className="row news-item  ">
      {NewsItems.map((item) => (
        <div key={item.id} className="pic-div-news col-md-6 col-lg-4 ">
          <div className="border">
            <Image
              height={414}
              className="img-1"
              style={{ width: "100%", height: "auto" }}
              src={item.pic}
              alt="Sunset in the mountains"
            />
            <div className="des-div-news">
              <div className="label">{item.label}</div>
              <p className="des">{item.des}</p>

              <Button title={t("read-more")} />
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default NewsComp;
