"use client";
import Link from "next/link";
import "./index.css";
import { useTranslation } from "react-i18next";

function Count() {
  const { t } = useTranslation("count");
  return (
    <>
      <section className="Count-sec">
        <div class="padding-count">
          <div class="row g-0">
            <div class="col-6 col-sm-6 col-md-6 col-lg-3">
              <Link className="link" href="#">
                <div class=" url-count-1 counter-box-filter">
                  <div class="d-sm-flex justify-content-sm-end align-items-sm-end counter-box">
                    <div class="counter-box-div">
                      <h2 class="counter-number">
                        <span class="fw-normal counter-title counter-title-span">
                          {t("active-representatives")}&nbsp;&nbsp;
                        </span>
                        <span class="count">2500</span>
                        <span class="count-number">+</span>
                      </h2>
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-3">
              <Link href="#">
                <div class="url-count-2 counter-box-filter">
                  <div class="d-sm-flex justify-content-sm-end align-items-sm-end counter-box">
                    <div class="counter-box-div">
                      <h2 class="counter-number">
                        <span class="fw-normal counter-title counter-title-span">
                          {t("executive-project")}&nbsp;
                        </span>
                        <span class="count">820</span>
                        <span class="count-number">+</span>
                      </h2>
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-3">
              <Link href="#">
                <div class="url-count-3 counter-box-filter">
                  <div class="d-sm-flex justify-content-sm-end align-items-sm-end counter-box">
                    <div class="counter-box-div">
                      <h2 class="counter-number">
                        <span class="fw-normal counter-title counter-title-span">
                          {t("loyal-customer")}&nbsp;
                        </span>
                        <span class="count">12600</span>
                        <span class="count-number">+</span>
                      </h2>
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-3">
              <Link href="#">
                <div class="url-count-4 counter-box-filter">
                  <div class="d-sm-flex justify-content-sm-end align-items-sm-end counter-box">
                    <div class="counter-box-div">
                      <h2 class="counter-number">
                        <span class="fw-normal counter-title counter-title-span">
                          {t("certificate")}&nbsp;
                        </span>
                        <span class="count">37</span>
                        <span class="count-number">+</span>
                      </h2>
                    </div>
                  </div>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default Count;
