import Link from "next/link";
import "../Representatives/index.css";
import { useTranslation } from "react-i18next";

function Representatives() {
  const { t } = useTranslation("namayandegi-forosh-content");
  return (
    <>
      <section className="Representatives">
        <div className="container">
          <div className="row ">
          
              <div className="col-sm-12 col-lg-6 res-col">
                <span>{t("access-green-representatives")}</span>
              </div>
              <div className="col-sm-12 col-lg-6" style={{textAlign:"left",marginTop:"10px"}}>
                <Link href="/namayandegi-forosh">
                  <button className="btn btn-1 strat-title-btn ">
                    {t("representatives-list")}
                  </button>
                </Link>
                <Link href={"/contact-us"}>
                  <button className="btn btn-2 strat-title-btn">
                    {t("representation-request")}
                  </button>
                </Link>
              </div>
        
          </div>
        </div>
      </section>
    </>
  );
}
export default Representatives;
