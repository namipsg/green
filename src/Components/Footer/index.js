"use client";
import "./index.css";
import Image from "next/image";
import Link from "next/link";
import { useTranslation } from "react-i18next";

function Footer({ menuData, products }) {
  const { t } = useTranslation("footer");

  return (
    <section className="footer-sec">
      <div className="container">
        <div className="row">
          <div className="col-md-12 col-lg-6 text-md-center text-lg-end">
            <div>
              <img src="/img/logo1.png" className="footer-logo" />
            </div>
            <div>
              <h6 className="fw-light footer-contact">
                <img
                  src="/img/icons8-place-marker-50.png"
                  className="footer-icon"
                />
                <span>
                  <span style={{ color: "rgb(255, 255, 255)" }}>
                    {t("headquarters")}
                  </span>
                </span>
              </h6>
              <h6 className="fw-light footer-contact">
                <img src="/img/icons8-hang-up-50.png" className="footer-icon" />
                <span>
                  <span style={{ color: "rgb(255, 255, 255)" }}>
                    {t("telephone")} : ۰۲۱۴۱۷۴۹&nbsp;
                  </span>
                </span>
              </h6>
              <h6 className="fw-light footer-contact">
                <img src="/img/icons8-fax-50.png" className="footer-icon" />
                <span>
                  <span style={{ color: "rgb(255, 255, 255)" }}>
                    {t("fax")} : ۰۲۱۸۸۸۹۰۱۶۴
                  </span>
                </span>
              </h6>
              <h6 className="fw-light footer-contact">
                <img src="/img/icons8-email-50.png" className="footer-icon" />
                <span>
                  <span style={{ color: "rgb(255, 255, 255)" }}>
                    {t("email")} : INFO@GREENAC.CO
                  </span>
                </span>
              </h6>
              <h6 className="fw-light footer-contact">
                <img
                  src="/img/icons8-place-marker-50.png"
                  className="footer-icon"
                />
                <span>
                  <span style={{ color: "rgb(255, 255, 255)" }}>
                    {t("central-exhibition")}
                  </span>
                </span>
              </h6>
              <h6 className="fw-light footer-contact">
                <img src="/img/icons8-hang-up-50.png" className="footer-icon" />
                <span>
                  <span style={{ color: "rgb(255, 255, 255)" }}>
                    {t("telephone")} : ۰۲۱۸۸۳۴۹۹۴۶
                  </span>
                </span>
              </h6>
              <h6 className="fw-light footer-contact">
                <img src="/img/icons8-fax-50.png" className="footer-icon" />
                <span>
                  <span style={{ color: "rgb(255, 255, 255)" }}>
                    {t("fax")} : ۰۲۱۸۸۸۹۰۱۶۴
                  </span>
                </span>
              </h6>
            </div>
            <div
              className="social-div"
              style={{ marginTop: 14, marginRight: 0 }}
            >
              <a href="#">
                <img src="/img/icons8-facebook-50.png" className="social-logo" />{" "}
              </a>
              <a href="#">
                <img
                  src="/img/icons8-instagram-50.png"
                  className="social-logo"
                />
              </a>
              <a href="#">
                <img src="/img/icons8-linkedin-50.png" className="social-logo" />{" "}
              </a>
              <a href="#">
                <img
                  src="/img/icons8-pinterest-50.png"
                  className="social-logo"
                />
              </a>
              <a href="#">
                <img
                  src="/img/icons8-twitter-squared-50.png"
                  className="social-logo"
                />
              </a>
              <a href="#">
                <img src="/img/icons8-youtube-50.png" className="social-logo" />
              </a>
            </div>
          </div>
          <div className="col-md-6 col-lg-3 text-md-center text-lg-end">
            <div className="footer-items-div">
              <ul>
                {menuData?.map((item) => (
                  <Link
                    href={item.url}
                    key={"footer_link_" + item.id}
                    className="text-white"
                  >
                    <li className="fw-light hover footer-items-links">
                      {item.name}
                    </li>
                  </Link>
                ))}
              </ul>
            </div>
          </div>
          <div className="col-md-6 col-lg-3 fw-light text-md-center text-lg-end">
            <div className="fw-light footer-items-div">
              <ul className="fw-light">
                {products.map((item) => (
                  <Link
                    href={"/products/" + item.id}
                    key={"footer_product_" + item.id}
                    className="text-white"
                  >
                    <li className="fw-light footer-items-links">{item.name}</li>
                  </Link>
                ))}
              </ul>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="footer-spacer" />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="text-center footer-copyright-div">
              <h6 className="fw-light footer-copyright">
                {t("website-design")}
              </h6>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export default Footer;
