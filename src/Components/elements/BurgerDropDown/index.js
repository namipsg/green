"use client";
import { Close } from "@mui/icons-material";
import { useState } from "react";
import "../BurgerDropDown/index.css";
import Link from "next/link";
import { useTranslation } from "react-i18next";

const BurgerDropDown = ({ title }) => {
  const { t } = useTranslation("navbar");
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const menuItems = [
    { id: 1, label: t("home"), link: "/" },
    { id: 2, label: t("about-us"), link: "/about-us" },
    { id: 3, label: t("images-gallery"), link: "/gallery" },
    { id: 4, label: t("contact-us"), link: "/contact-us" },
  ];

  return (
    <div className="burger-menu-container">
      <button className="btn" type="button" onClick={toggleDropdown}>
        {title}
      </button>
      {isOpen && (
        <div className="Burger-side">
          <div className="burger-side-ul">
            {/* Dropdown content goes here */}
            <button onClick={toggleDropdown} className="btn close-btn">
              <Close />
            </button>

            <div className="ul-div">
              <ul className="ul">
                {menuItems.map((item) => (
                  <li key={item.id} className="li">
                    <Link className="label-burger" href={item.link}>
                      {item.label}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className=""></div>
          </div>
        </div>
      )}
    </div>
  );
};
export default BurgerDropDown;
