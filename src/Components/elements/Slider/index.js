"use client";
import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import "./index.css";

// import required modules
import { Autoplay, Pagination, Navigation } from "swiper/modules";
import pic from "../../../public/img/bb1.jpg";
import Image from "next/image";
import About_us from "@/app/about-us/page";
import { Close } from "@mui/icons-material";
import Link from "next/link";

export default function Slider({ slidesPerView, sliderdata }) {
  const [swiperRef, setSwiperRef] = useState(null);
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <Swiper
        dir="rtl"
        grabCursor={true}
        onSwiper={setSwiperRef}
        slidesPerView={slidesPerView}
        centeredSlides={false}
        spaceBetween={30}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          0: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          1024: {
            slidesPerView: slidesPerView,
            spaceBetween: 50,
          },
        }}
        navigation={true}
        modules={[Autoplay, Pagination, Navigation]}
        className="mySwiper myswiper"
      >
        {sliderdata.map((item) => (
          <SwiperSlide className="swiperslide" key={"slide_" + item.id}>
            <div className="slider-box">
              <Link href="#">
                <div className="slider-box-pic">
                  <Image src={item.pic} className="pic" alt="pic" />
                </div>
                <div className="slider-data-box">
                  <div className="slider-data-top">
                    <h4 className="slider-data-title">{item.title}</h4>
                    <p className="slider-data-des">{item.des}</p>
                  </div>
                  <h5 className="slider-data-city">{item.city}</h5>
                </div>
              </Link>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
