import  '../MidVideo/index.css'


function MidVideo() {
  return (
    <>
      <section className="Video-sec-mid">
        <video autoPlay loop muted className="video-mid">
          <source src='/video/Speaker.mp4' type="video/mp4"/>
        </video>
      </section>
    </>
  );
}
export default MidVideo;
