import { PANELURL } from "../../../../config/default";
import "../Cards/index.css";
import Link from "next/link";

function Cards({ data }) {
  return (
    <>
      <section className="Cards">
        <div className="container">
          <div className="d-flex justify-content-center align-items-stretch flex-wrap">
            {data?.map((item) => {
              return (
                <Link
                  className="link flex h-10"
                  href={`products/${item.id}`}
                  key={"product" + item.id}
                >
                  <div className="d-flex col-1 col-md-3 card-row h-100">
                    <div className="pic-div h-100 d-flex flex-column">
                      {item.image ? (
                        <img
                          src={PANELURL + item.image}
                          alt={item.title}
                          className="pic mt-auto mb-auto"
                          style={{ width: "100%", height: "auto" }}
                        />
                      ) : (
                        <div className="border rounded-3 p-5 mt-auto mb-auto">
                          <img
                            src="/img/logo1.png"
                            alt={item.title}
                            className="pic "
                            style={{
                              width: "100%",
                              height: "auto",
                              maxWidth: 120,
                            }}
                          />
                        </div>
                      )}
                      <div className="mt-auto p-2"></div>
                      <h5 className="text-center">{item.name}</h5>
                    </div>
                  </div>
                </Link>
              );
            })}
          </div>
        </div>
      </section>
    </>
  );
}
export default Cards;
