import "../Button/index.css";

export const Button = ({ title }) => {
  return (
    <>
      <button className="General-btn ">
        <span className="general-btn-text">{title}</span>
      </button>
    </>
  );
};
