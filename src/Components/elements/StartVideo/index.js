import "./index.css";
import Link from "next/link";
import { useTranslation } from "react-i18next";

function StartVideo() {
  const { t } = useTranslation("start-video");
  return (
    <>
      <section className="Video-sec">
        <video autoPlay loop muted className="video">
          <source src="/video/home.mp4" type="video/mp4" className="video-1" />
        </video>
        <div className="container ">
          <div className="row">
            <div className="col-md-8">
              <div className="title">
                <h1 className="first-p ">{t("green-air-conditioning")}</h1>
                <h1 className="h1">{t("green-first-last")}</h1>
                <h3 className="second-p ">{t("description")}</h3>
                <p className="third-p ">{t("knowledge")}</p>
                <button className="btn btn-success  ">
                  <Link className="start-title-btn" href="about-us">
                    {t("know-more")}
                  </Link>
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default StartVideo;
