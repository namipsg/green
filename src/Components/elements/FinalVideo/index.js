import '../FinalVideo/index.css'


function MidVideo() {
  return (
    <>
      <section className="Video-sec-final">
        <video autoPlay loop muted className="video-final">
          <source src='/video/Vrf.mp4' type="video/mp4" />
        </video>
      </section>
    </>
  );
}
export default MidVideo;
