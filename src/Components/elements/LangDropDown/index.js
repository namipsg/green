"use client";

import React, { useState } from "react";
import FlagIran from "../../../public/img/iranf.png";
import FlgUk from "../../../public/img/pngegg.png";
import { ExpandLessOutlined, ExpandMoreOutlined } from "@mui/icons-material";
import "../LangDropDown/index.css";
import Image from "next/image";
import { useTranslation } from "react-i18next";

const LangDropDown = ({ title }) => {
  const { t } = useTranslation("navbar");
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <div className="dropdown">
        <button onClick={toggleDropdown} className="DropDown">
          <div className="DropDown_div">
            <div>
              <span>
                <Image
                  width={30}
                  src={FlagIran}
                  alt="flag-iran"
                  className="flag"
                />
                {t("persian")}
              </span>
            </div>
            <div>
              <ExpandMoreOutlined />
            </div>
          </div>
        </button>
        {isOpen && (
          <button onClick={toggleDropdown} className="DropDown" id="DropDown">
            <div className="DropDown_div">
              <div>
                <span>
                  <Image
                    width={30}
                    height={30}
                    src={FlgUk}
                    alt="flag-iran"
                    className="flag"
                  />
                  {t("english")}
                </span>
              </div>
              <div>
                <ExpandLessOutlined />
              </div>
            </div>
          </button>
        )}
      </div>
    </>
  );
};

export default LangDropDown;
