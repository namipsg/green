import "../Card/index.css";

const { default: Image } = require("next/image");

function Card({ CardItem }) {
  return (
    <>
      {CardItem.map((item) => (
        <div key={item.id} className="col-md-4">
          <div className="Card-box ">
            <div className="card-box-img">
              <Image src={item.pic} alt="pic" className="card-pic" />
            </div>
            <div className="card-box-title">
              <h5>{item.title}</h5>
            </div>
            <div className="card-box-des">
              <p className="card-p">{item.des}</p>
            </div>
          </div>
        </div>
      ))}
    </>
  );
}
export default Card;
