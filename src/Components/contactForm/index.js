"use client";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { enqueueSnackbar } from "notistack";
import React from "react";
import { useState } from "react";
import { useTranslation } from "react-i18next";

const ContactForm = () => {
  const { t } = useTranslation("contact-us");
  const { t: et } = useTranslation("error");
  const schema = yup
    .object({
      name: yup
        .string()
        //   .min(3, "نام باید حداقل ۳ حرفی باشد")
        .required(t("name-required")),
      mobile: yup
        .string()
        .required(t("mobileNumber-required"))
        .min(11, t("mobileNumber-min"))
        .max(11, t("mobileNumber-max")),
      email: yup
        .string()
        .email(t("email-validation"))
        .required(t("email-required")),
      subject: yup.string().required(t("subject-required")),
      context: yup.string().required(t("message-text")),
    })
    .required();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    setIsLoading(true);
    setTimeout(() => {
      setIsLoading(false);
      enqueueSnackbar(et("request-successfull-submit"), {
        variant: "success",
      });
    }, 1000);
  };

  const [isLoading, setIsLoading] = useState(false);
  return (
    <>
      <section>
        <div className="data">
          <div className="container">
            <form
              className="d-flex flex-column gap-3"
              onSubmit={handleSubmit(onSubmit)}
            >
              <div className="d-flex gap-3">
                <div className="flex-fill">
                  <label htmlFor="name" className="d-block m-2">
                    {t("name")}
                  </label>
                  <input
                    {...register("name")}
                    style={{
                      width: "100%",
                      paddingTop: "5px",
                      paddingBottom: "5px",
                      borderRadius: "5px",
                      border:
                        "var(--bs-border-width) solid var(--bs-border-color)",
                    }}
                  />
                  <p className="text-danger">{errors.name?.message}</p>
                </div>
                <div className="flex-fill">
                  <label htmlFor="mobile" className="d-block m-2">
                    {t("mobile")}
                  </label>
                  <input
                    {...register("mobile")}
                    style={{
                      width: "100%",
                      paddingTop: "5px",
                      paddingBottom: "5px",
                      borderRadius: "5px",
                      border:
                        "var(--bs-border-width) solid var(--bs-border-color)",
                    }}
                  />
                  <p className="text-danger">{errors.mobile?.message}</p>
                </div>
              </div>
              <div className="d-flex gap-3">
                <div className="flex-fill">
                  <label htmlFor="email" className="d-block m-2">
                    {t("email")}
                  </label>
                  <input
                    {...register("email")}
                    style={{
                      width: "100%",
                      paddingTop: "5px",
                      paddingBottom: "5px",
                      borderRadius: "5px",
                      border:
                        "var(--bs-border-width) solid var(--bs-border-color)",
                    }}
                  />
                  <p className="text-danger">{errors.email?.message}</p>
                </div>
                <div className="flex-fill">
                  <label htmlFor="subject" className="d-block m-2">
                    {t("subject")}
                  </label>
                  <input
                    {...register("subject")}
                    style={{
                      width: "100%",
                      paddingTop: "5px",
                      paddingBottom: "5px",
                      borderRadius: "5px",
                      border:
                        "var(--bs-border-width) solid var(--bs-border-color)",
                    }}
                  />
                  <p className="text-danger">{errors.subject?.message}</p>
                </div>
              </div>
              <div className="flex-fill">
                <label htmlFor="context" className="d-block">
                  {t("message")}
                </label>
                <textarea className="w-100" {...register("context")} rows={4} />
                <p className="text-danger">{errors.context?.message}</p>
              </div>

              <button
                className="btn btn-success w-lg-100 w-25"
                disabled={isLoading}
              >
                {t("submit")}
              </button>
            </form>
          </div>
        </div>
      </section>
    </>
  );
};
export default ContactForm;
