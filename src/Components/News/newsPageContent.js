"use client";
import * as React from "react";
import "./index.css";
import { Button } from "../elements/Button";
import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";
import Config, { PANELURL } from "../../../config/default";
import { Autoplay, Navigation, Pagination } from "swiper/modules";
import { useTranslation } from "react-i18next";
import "./index1.css";
import PagePagination from "./navigation";
import "swiper/css";

const NewsPageContent = ({ categoryData, newsList, newsData }) => {
  const { t } = useTranslation("news");
  const [isLoading, setIsLoading] = React.useState(false);
  const [news, setNews] = React.useState(newsData);

  const getNewsList = async (id) => {
    setIsLoading(true);
    try {
      const newsList = await fetch(
        Config.API.NEWS.GETALL + "?categoryId=" + id,
        {
          cache: "no-store",
        }
      );
      const data = await newsList.json();
      setNews(data);
    } catch (error) {}
    setIsLoading(false);
  };

  return (
    <>
      <section>
        <div
          className="top-pic"
          style={{
            width: "100vw",
            height: "calc(100vh - 40vh)",
            backgroundSize: "cover",
            backgroundImage: 'url("/img/headers/news.jpg")',
            backgroundRepeat: "no-repeat",
            zIndex: "-10",
          }}
        >
          <div className="container">
            <div className="row">
              <div className="col-6">
                <div className="title-box col-12">
                  <h2 className="title-h font-800">{t("news-articles")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="pad-50">
        <div className="special-news">
          <div className="container ">
            <div className="row">
              <div className="special-news-h">
                <hr className="flex-fill hr-news" />
                <h4 className="font-800">{t("special-news")}</h4>
                <hr className="flex-fill hr-news" />
              </div>
              <div className="special-news-slider">
                <Swiper
                  dir="rtl"
                  slidesPerView={2}
                  spaceBetween={30}
                  autoplay={{
                    delay: 2500,
                    disableOnInteraction: false,
                  }}
                  pagination={{
                    clickable: true,
                  }}
                  breakpoints={{
                    250: {
                      slidesPerView: 1,
                      spaceBetween: 50,
                    },
                    768: {
                      slidesPerView: 2,
                      spaceBetween: 50,
                    },                 
                    1024: {
                      slidesPerView: 2,
                      spaceBetween: 50,
                    },
                  }}
                  navigation={true}
                  modules={[Autoplay, Pagination, Navigation]}
                  className="mt-5"
                >
                  {newsData?.data?.slice(0, 7).map((item) => (
                    <SwiperSlide
                      className="swiperslide"
                      key={"slide_" + item.id}
                    >
                      <Link href={"/news/" + item.id}>
                        <div className="slider-box">
                          <div className="slider-box-pic">
                            <img
                              src={PANELURL + item.mainPhoto}
                              className="img-fluid w-100 rounded"
                              alt={item.title}
                              style={{
                                aspectRatio: "16/9",
                                objectFit: "cover",
                              }}
                            />
                          </div>
                          <div className="slider-data-box mt-3">
                            <div className="slider-data-top">
                              <h4 className="slider-data-title">{item.title}</h4>
                              <p className="slider-data-des">{item.lead}</p>
                            </div>
                          </div>
                        </div>
                      </Link>
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
            </div>
          </div>
        </div>
        <div className="news-archive">
          <div className="container">
            <div className="row">
              <div className="news-archive-title col-12">
                <hr className="flex-fill hr-news" />
                <h4 className="news-archive-h font-800">{t("news-archive")}</h4>
                <hr className="flex-fill hr-news" />
              </div>
              <div className="d-flex gap-3 overflow-auto justify-content-lg-center w-100 category-div">
                <div
                  className="news-archive-part"
                  onClick={() => getNewsList("")}
                >
                  <Button title="همه اخبار">
                    <Link href={"#"} />
                  </Button>
                </div>
                {categoryData?.data?.map((item) => {
                  return (
                    <div
                      className="news-archive-part"
                      key={"archive_" + item.id}
                      onClick={() => getNewsList(item.id)}
                    >
                      <Button title={item.name}>
                        <Link href={"#"} />
                      </Button>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="d-flex flex-wrap">
              {news?.data?.length > 0 ? (
                news?.data?.map((item) => (
                  <Link
                    href={"/news/" + item.id}
                    key={"archive_" + item.id}
                    className={
                      isLoading ? "col-md-4 p-4 opacity-50" : "col-md-4 p-4"
                    }
                  >
                    <div className="Card-box w-100">
                      <div className="card-box-img w-100">
                        <img
                          src={PANELURL + item.mainPhoto}
                          alt={item.title}
                          className="img-fluid rounded w-100"
                          style={{
                            aspectRatio: "16/9",
                            objectFit: "cover",
                          }}
                        />
                      </div>
                      <div className="card-box-title mt-4">
                        <h5>{item.title}</h5>
                      </div>
                      <div className="card-box-des des des2">{item.description}</div>
                    </div>
                  </Link>
                ))
              ) : (
                <div class="alert alert-primary w-100 m-5 text-center" role="alert">
                  موردی برای نمایش وجود ندارد
                </div>
              )}
            </div>
            {/* <div
              style={{
                display: "flex",
                justifyContent: "center",
                direction: "ltr",
                marginTop: "50px",
              }}
            >
              <PagePagination />
            </div> */}
            {/* <div className="news-page-num-box">
              <button className="news-page-per">
                <arrowRight />
              </button>
              <div className="news-page-num">
                <div className="news-counter-btn">
                  <button>1</button>
                </div>
                <div className="news-counter-btn">
                  <button>2</button>
                </div>
                <div className="news-counter-btn">
                  <button>3</button>
                </div>
                <div className="news-counter-btn">
                  <button>4</button>
                </div>
                <div className="news-counter-btn">
                  <button>5</button>
                </div>
              </div>
              <button className="news-page-next">
                <arrowLeft />
              </button>
            </div> */}
          </div>
        </div>
      </section>
    </>
  );
};
export default NewsPageContent;
