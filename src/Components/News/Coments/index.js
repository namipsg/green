import "../Coments/index.css";
import { useTranslation } from "react-i18next";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { useState } from "react";
import Config from "../../../../config/default";
import { enqueueSnackbar } from "notistack";

function Coments({ postId }) {
  const { t } = useTranslation("news");
  const schema = yup
    .object({
      description: yup
        .string()
        .min(5, "متن پیام باید حداقل ۵ حرفی باشد")
        .required("متن پیام الزامی می باشد"),
      name: yup
        .string()
        .min(3, "نام شما حداقل باید ۳ حرفی باشد")
        .required("نام شما الزامی می باشد"),
      family: yup
        .string()
        .min(3, "نام خانوادگی شما حداقل باید ۳ حرفی باشد")
        .required("نام خانوادگی شما الزامی می باشد"),
    })
    .required();

  const [isLoading, setIsLoading] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (formData) => {
    setIsLoading(true);

    try {
      const { name, family, description } = formData;
      const response = await fetch(Config.API.NEWS.COMMENT, {
        method: "POST",
        body: JSON.stringify({
          postId,
          name,
          family,
          description,
        }),
        headers: {
          ["Content-Type"]: "application/json",
        },
      });

      if (response.status === 200) {
        reset();
        enqueueSnackbar("نظر شما با موفقیت ثبت گردید", {
          variant: "success",
        });
      } else {
        enqueueSnackbar("خطا در حین ارسال اطلاعات", {
          variant: "error",
        });
      }
    } catch (error) {
      enqueueSnackbar(error.message || "خطا در حین ارسال اطلاعات", {
        variant: "error",
      });
    }
    setIsLoading(false);
  };
  return (
    <>
      <div className="container ">
        <div className="row">
          <div className="col-12 comment-title-box">
            <span className="comment-title-h">{t("user-comments")}</span>
          </div>
          <form
            onSubmit={handleSubmit(onSubmit)}
            className={isLoading ? "opacity-50 row" : "row"}
          >
            <div className="col-12 first-input-box mb-0">
              <textarea className="first-input" {...register("description")} />
            </div>
            <p className="text-danger my-0">{errors.description?.message}</p>
            <div className="col-md-5 second-input-box">
              <input
                type="text"
                placeholder={t("name")}
                className="second-input mb-0"
                {...register("name")}
              />
              <p className="text-danger">{errors.name?.message}</p>
            </div>
            <div className="col-md-5 third-input-box">
              <input
                type="text"
                placeholder={"نام خانوادگی"}
                className="third-input mb-0"
                {...register("family")}
              />
              <p className="text-danger">{errors.family?.message}</p>
            </div>
            <div className="col-md-2 fourth-input-box">
              <input
                type="submit"
                value={t("submit")}
                className="fourth-input"
              />
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
export default Coments;
