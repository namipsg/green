'use client'
import React from 'react';
import { PANELURL } from '../../../config/default';
import './index1.css'
import './index.css'
import Coments from './Coments';
import {
  EmailIcon,
  FacebookMessengerIcon,
  TelegramIcon,
  TwitterIcon,
  WhatsappIcon,
} from "react-share";
import { IconButton } from "@mui/material";

const ViewNews = ({ data }) => {
  return (
    <section className="pad-50">
      <div className="top-pic" style={{
        background: `url(${PANELURL + data?.mainPhoto})`,
        backgroundSize: 'cover'
      }}>
        <div className="container ">
          <div className="row">
            <div className="col6">
              <div className="title-box">
                <h2 className="title-h font-800">{data?.title}</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="New-top-box">
        <div className="container ">
          <div className="row pack">
            <div className="col-md-6 new-data-box">
              <p className="new-p">{data?.description}</p>
            </div>
            <div className="col-md-6 new-pic-box">
              <img
                src={PANELURL + data?.mainPhoto}
                alt={data?.title}
                className="new-pic"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="News-mid-box">
        <div className="container ">
          <div className="row">
            <div className="col-12"> 
              <div
                className="news-mid-p"
                dangerouslySetInnerHTML={{ __html: data?.content }}
              ></div>
            </div>
          </div>
        </div>
      </div>
      <div className="Share-us">
        <div className="container ">
          <div className="row">
            <div className="col-12">
              <div className="share-div">
                <span className="share-us-title ">به اشتراک بگذارید</span>
                <div className="container share-icons-cont d-flex gap-1">
                  <IconButton href={`sms://body=Check out this site ${window.location.href}`}
                    title="I wanted you to see this site">
                    <FacebookMessengerIcon size={32} round />
                  </IconButton>
                  <IconButton href={`http://twitter.com/share?text=Check out this site&url=${window.location.href}`}>
                    <TwitterIcon size={32} round />
                  </IconButton>
                  <IconButton href={`https://telegram.me/share/url?url=${window.location.href}&text=Check out this site`}>
                    <TelegramIcon size={32} round />
                  </IconButton>
                  <IconButton href={`whatsapp://send?text=Check out this site ${window.location.href}`}
                    data-action="share/whatsapp/share">
                    <WhatsappIcon size={32} round />
                  </IconButton>
                  <IconButton href={`mailto:?subject=I wanted you to see this site&body=Check out this site
          ${window.location.href}`}>
                    <EmailIcon size={32} round />
                  </IconButton>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="Comments mt-5" >
        <Coments postId={data?.id} />
      </div>
    </section>
  );
}

export default ViewNews; { }