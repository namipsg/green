// import NewsComp from "../NewsComp/index.js";
import "../News/index.css";
import "../News/index1.css";
import Link from "next/link";
import { PANELURL } from "../../../config/default";
import { useTranslation } from "react-i18next";
import { Button } from "../elements/Button";

function News({ data }) {
  const { t } = useTranslation("news");
  return (
    <div className="News">
      <div className="container">
        <div className="row">
          <div className="h2">{t("news-articles")}</div>
          <div className="p news-p">{t("latest-news-HVAC")}</div>
        </div>

        <div className="d-flex flex-wrap">
          {data?.map((item) => (
            <div
              key={item.id}
              className="pic-div-news col-md-6 col-lg-4 flex p-3"
            >
              <div className="border">
                <img
                  className="img-fluid"
                  src={PANELURL + item.mainPhoto}
                  alt={item.title}
                  style={{
                    width: "100%",
                    height: "300px",
                    objectFit: "cover",
                    position: "relative",
                  }}
                />
                <div className="des-div-news">
                  <div className="label mb-3">{item.title}</div>
                  <p className="des">{item.lead}</p>

                  <Link href={`/news/${item.id}`}>
                    <Button title={t("read-more")} />
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
export default News;
