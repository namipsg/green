"use client";

import { useEffect } from "react";

function Bootstrap() {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.js");
  }, []);

  return null;
}

export default Bootstrap;
