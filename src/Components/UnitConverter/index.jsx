"use client";
import Image from "next/image";
import { Email, Fax, Phone } from "@mui/icons-material";
import Link from "next/link";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { enqueueSnackbar } from "notistack";
import React, { useEffect } from "react";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import Config from "../../../config/default";

function UnitConverter({ units }) {
  const { t } = useTranslation("unit-converter");
  const schema = yup
    .object({
      unit: yup.string().required(),
      from: yup.string().required(),
      value: yup.number().required(),
      to: yup.string().required(),
    })
    .required();

  const unitGuide = {
    Length: t("length"),
    1: t("weight"),
    Time: t("time"),
    Temperature: t("temperature"),
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
    setValue
  } = useForm({
    resolver: yupResolver(schema),
  });

  const [values, setValues] = useState({});
  const [unitTypes, setUnitTypes] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [result, setResult] = useState(null)

  const getSubs = async (id) => {
    try {
      const response = await fetch(Config.API.UNIT.GETSUBS + `?typeId=${id}`);
      if (response.status === 200) {
        const jsonResponse = await response.json();
        const { data } = jsonResponse;
        setUnitTypes(data);
      }
    } catch (error) {

    }
  }

  const checkForm = async () => {
    setTimeout(() => { onSubmit(getValues()) }, 100)
  }

  const onSubmit = async (data) => {
    setLoading(true);
    try {
      const validated = await schema.validate(data);
      if (validated.from == validated.to) {
        setResult(validated.value)
      } else {
        const { from, to } = validated
        const response = await fetch(Config.API.UNIT.CONVERT + `?quantityId1=${from}&quantityId2=${to}`);
        const responseJson = await response.json();
        if (response.status === 200) {
          const { data } = responseJson;
          setResult(validated.value * data.quantity)
        } else {
          enqueueSnackbar(responseJson.message?.[0] || "مشکلی در حین محاسبه پیش آمده")
        }
      }
    } catch (error) {
    }
    setLoading(false)
  };

  return (
    <>
      <section>
        <div className="top-foroosh">
          <div className="container ">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">
                    {t("online-unit-converter")}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="container mx-auto p-5">
        <form
          onChange={() => setValues(getValues())}
          className="col-12 col-md-6 d-flex flex-column gap-3 border rounded p-3"
          style={{
            backgroundColor: 'whitesmoke',
            opacity: isLoading ? .5 : 1
          }}

        >
          <div className="flex-fill">
            <FormControl fullWidth>
              <InputLabel id="province-label">{t("quantity")}</InputLabel>
              <Select
                labelId="province-label"
                id="province-select"
                label={t("quantity")}
                placeholder={t("province")}
                {...register("unit")}
                sx={{
                  background: '#fff'
                }}
              >
                {units?.data?.map((unit) => {
                  return (
                    <MenuItem
                      sx={{ color: "black" }}
                      value={unit.id}
                      key={"unit_" + unit.id}
                      onClick={() => getSubs(unit.id)}
                    >
                      {unit.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </div>
          <div className="w-100 d-flex gap-2 align-items-center">
            <hr className="col-1" />
            <p className="m-0">{unitGuide[getValues().unit]}</p>
            <hr className="flex-fill" />
          </div>
          <div className="flex-fill">
            <FormControl fullWidth>
              <InputLabel id="province-label">{t("convert-from")}</InputLabel>
              <Select
                labelId="province-label"
                id="province-select"
                label={t("convert-from")}
                placeholder={t("convert-from")}
                {...register("from")}
                disabled={unitTypes.length < 1}
                sx={{
                  background: '#fff'
                }}
              >
                {unitTypes?.map((unit) => {
                  return (
                    <MenuItem
                      sx={{ color: "black" }}
                      value={unit.id}
                      key={"unit_" + unit.id}
                      onClick={checkForm}
                    >
                      {unit.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </div>
          <div className="flex-fill">
            <TextField variant="outlined" fullWidth sx={{
              background: '#fff',
              my: 2
            }} type="number" label={t("amount")} placeholder={t("amount")} value={getValues().value} onChange={(e) => {
              setValue("value", e.target.value);
              checkForm();
            }} />
          </div>
          <div className="flex-fill">
            <FormControl fullWidth>
              <InputLabel id="province-label">{t("convert-to")}</InputLabel>
              <Select
                labelId="province-label"
                id="province-select"
                label={t("convert-to")}
                placeholder={t("convert-to")}
                {...register("to")}
                disabled={unitTypes.length < 1}
                sx={{
                  background: '#fff'
                }}
              >
                {unitTypes?.map((unit) => {
                  return (
                    <MenuItem
                      sx={{ color: "black" }}
                      value={unit.id}
                      key={"unit_" + unit.id}
                      onClick={checkForm}
                    >
                      {unit.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </div>
          <hr />
          <label className="mb-2">{t("result")}</label>
          <input type="text" className="form-control" readOnly value={result < 0 ? result : result?.toLocaleString()} />
        </form>
      </section>
    </>
  );
}
export default UnitConverter;
