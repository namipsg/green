"use client";
import "./index.css";
import Link from "next/link";
import { useTranslation } from "react-i18next";
import { BASEURL, PANELURL } from "../../../../config/default";

const GalleryGroup = ({ groupsData }) => {
  const { t } = useTranslation("gallery-group");
  return (
    <>
      <section>
        <div className="top-pic">
          <div className="container">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">{t("gallery")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="pad-50">
        <div className="gallery">
          <div className="container">
            <div className="row">
              {
                groupsData?.data?.map((item) => <Link key={'gal_' + item.id} href={item.hasChild === true ? `/gallery/${item.id}` : `/gallery/view/${item.id}`}>
                  <div className="gallery-box">
                    <div className="gallery-title-box">
                      <h4>{item.name}</h4>
                    </div>
                    <div className="gallery-image-box">
                      <img
                        src={item.imageUri ? PANELURL + item.imageUri : '/img/bb1.jpg'}
                        alt="pic"
                        className="gallery-pic rounded border w-100"
                        style={{
                          aspectRatio: '16/9'
                        }}
                      />
                    </div>
                  </div>
                </Link>)
              }
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default GalleryGroup;
