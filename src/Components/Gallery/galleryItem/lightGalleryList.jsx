"use client";
import React from "react";
import { PANELURL } from "../../../../config/default";
import LightGallery from 'lightgallery/react';

// import styles
import 'lightgallery/css/lightgallery.css';
import 'lightgallery/css/lg-zoom.css';
import 'lightgallery/css/lg-thumbnail.css';

// If you want you can use SCSS instead of css
import 'lightgallery/scss/lightgallery.scss';
import 'lightgallery/scss/lg-zoom.scss';

// import plugins if you need
import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgZoom from 'lightgallery/plugins/zoom';
import lgVideo from 'lightgallery/plugins/video';
import "./index.css";

const LightGalleryList = ({ data }) => {
  const onInit = () => {
    console.log("lightGallery has been initialized");
  };

  return (
    <div className="Layout-3">
      <div className="container">
        <div className="row">
          {data?.length > 0 ? <LightGallery onInit={onInit} speed={500} plugins={[lgZoom, lgVideo]}>
            {data?.map((item) => (
              <div data-src={`${PANELURL}${item.imageUrl}`} key={item.id} className="col-md-3 d-inline-block m-3">
                {item.contentType === 1 ? (<img
                  src={`${PANELURL}${item.imageUrl}`}
                  alt={item.name}
                  className="card-pic"
                />) : (
                  <video className="card-pic" controls>
                    <source
                      src={`${PANELURL}${item.imageUrl}`}
                      alt={item.name}

                    />
                  </video>
                )}
                <p>{item.name}</p>
              </div>
            ))}
          </LightGallery> : (
            <div class="alert alert-primary w-100 m-5 text-center" role="alert">
              موردی برای نمایش وجود ندارد
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default LightGalleryList;
