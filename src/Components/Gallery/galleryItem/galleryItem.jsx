"use client";
import "./index.css";
import Layout from "@/layout";
import LightGalleryList from "./lightGalleryList";
import { useTranslation } from "react-i18next";
import { PANELURL } from "../../../../config/default";

function GalleryItem({ finalData }) {
  const { t } = useTranslation("gallery-group");
  return (
    <>
      <section>
        <div className="top-pic" style={
          finalData?.data?.length > 0
            ? {
              background: `url(${PANELURL}/${finalData.data[0].imageUrl})`,
              backgroundSize: "cover",
              backgroundPosition: "center",
            }
            : null
        }>
          <div className="container ">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">{t("gallery")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <LightGalleryList data={finalData?.data} />
      </section>
    </>
  );
}
export default GalleryItem;
