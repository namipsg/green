"use client";
import "./index.css";
import { useTranslation } from "react-i18next";
import { PANELURL } from "../../../../config/default";
import Link from "next/link";

function GalleryContent({ data, id }) {
  const { t } = useTranslation("gallery-group");

  return (
    <>
      <section>
        <div
          className="top-pic"
          style={
            data?.data?.length > 0
              ? {
                  background: `url(${PANELURL}/${data.data[0].imageUrl})`,
                  backgroundSize: "cover",
                  backgroundPosition: "center",
                }
              : null
          }
        >
          <div className="container ">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">{t("image-gallery")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="layout2">
          <div className="container">
            <div className="d-flex flex-wrap">
              {data?.data && data.data.length > 0 ? (
                [
                  data.data?.map((parent) => (
                    <div className="col-6" key={parent.id}>
                      <Link href={`/gallery/${id}/${parent.id}`}>
                        <div className="layout2-pic-box">
                          <img
                            className="layout2-pic rounded border"
                            src={`${PANELURL}/${parent.imageUrl}`}
                            alt={parent.name}
                          />
                          <h5 className="mt-3 text-center">{parent.name}</h5>
                        </div>
                      </Link>
                    </div>
                  )),
                ]
              ) : (
                <div
                  class="alert alert-primary w-100 m-5 text-center"
                  role="alert"
                >
                  موردی برای نمایش وجود ندارد
                </div>
              )}
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default GalleryContent;
