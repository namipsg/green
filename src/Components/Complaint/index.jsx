"use client";
import { set, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { enqueueSnackbar } from "notistack";
import React from "react";
import { useState } from "react";
import Config from "../../../config/default";
import { DatePicker } from "zaman";
import { useTranslation } from "react-i18next";

function Complaint() {
  const { t } = useTranslation("complaint");
  const { t: et } = useTranslation("error");
  const schema = yup
    .object({
      firstname: yup
        .string()
        .min(3, t("firstname-min"))
        .required(t("firstname-required")),
      lastname: yup
        .string()
        .min(3, t("lastname-min"))
        .required(t("lastname-required")),
      mobileNumber: yup
        .string()
        .required(t("mobileNumber-required"))
        .min(11, t("mobileNumber-min"))
        .max(11, t("mobileNumber-max")),
      phoneNumber: yup
        .string()
        .required(t("phoneNumber-required"))
        .min(11, t("phoneNumber-min"))
        .max(11, t("phoneNumber-max")),

      nationalCode: yup
        .string()
        .required(t("nationalCode-required"))
        .min(10, t("nationalCode-min"))
        .max(10, t("nationalCode-max")),
      address: yup.string().required(t("address-required")),
      email: yup
        .string()
        .email(t("email-validation"))
        .required(t("email-required")),
      setDate: yup.date().required(t("setDate-required")),
      brand: yup.string().required(t("brand-required")),
      type: yup.string().required(t("type-required")),
      serial: yup.string().required(t("serial-required")),
      complaint: yup.string().required(t("complaint-required")),
    })
    .required();
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (formData) => {
    try {
      setIsLoading(true);
      const response = await fetch(Config.API.COMPLAINT.POST, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      const data = await response.json();
      if (response.status !== 200) {
        enqueueSnackbar(
          data.message?.[0] || et("complaint-registration-error"),
          { variant: "error" }
        );
        setIsLoading(false);
      } else {
        enqueueSnackbar(et("complaint-registration-success"), {
          variant: "success",
        });
        reset();
        setIsLoading(false);
      }
    } catch (error) {
      enqueueSnackbar(et("complaint-registration-error"), { variant: "error" });
      setIsLoading(false);
    }
  };

  const [isLoading, setIsLoading] = useState(false);

  return (
    <div id="complaint">
      <section>
        <div className="top-pic">
          <div className="container ">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">{t("complaint-submit")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="data">
          <div className="container">
            <h4>{t("complaint-form")}</h4>
            <p>{t("complaint-form-complete")}</p>
            <form
              className="d-flex flex-column gap-3"
              onSubmit={handleSubmit(onSubmit)}
            >
              <div className="d-flex gap-3">
                <div className="flex-fill">
                  <label htmlFor="firstname" className="d-block">
                    {t("first-name")}
                  </label>
                  <input {...register("firstname")} />
                  <p className="text-danger">{errors.firstname?.message}</p>
                </div>
                <div className="flex-fill">
                  <label htmlFor="lastname" className="d-block">
                    {t("last-name")}
                  </label>
                  <input {...register("lastname")} />
                  <p className="text-danger">{errors.lastname?.message}</p>
                </div>
              </div>
              <div className="d-flex gap-3">
                <div className="flex-fill">
                  <label htmlFor="mobileNumber" className="d-block">
                    {t("mobile")}
                  </label>
                  <input {...register("mobileNumber")} />
                  <p className="text-danger">{errors.mobileNumber?.message}</p>
                </div>
                <div className="flex-fill">
                  <label htmlFor="phoneNumber" className="d-block">
                    {t("phone-number")}
                  </label>
                  <input {...register("phoneNumber")} />
                  <p className="text-danger">{errors.phoneNumber?.message}</p>
                </div>
              </div>
              <div className="flex-fill">
                <label htmlFor="nationalCode" className="d-block">
                  {t("national-code")}
                </label>
                <input {...register("nationalCode")} />
                <p className="text-danger">{errors.nationalCode?.message}</p>
              </div>
              <div className="flex-fill">
                <label htmlFor="address" className="d-block">
                  {t("address")}
                </label>
                <input {...register("address")} />
                <p className="text-danger">{errors.address?.message}</p>
              </div>
              <div className="flex-fill">
                <label htmlFor="email" className="d-block">
                  {t("email")}
                </label>
                <input {...register("email")} />
                <p className="text-danger">{errors.email?.message}</p>
              </div>
              <div className="flex-fill">
                <label htmlFor="setDate" className="d-block">
                  {t("installation-date")}
                </label>
                <DatePicker
                  onChange={(date) => setValue("setDate", new Date(date.value))}
                />
                <p className="text-danger">{errors.setDate?.message}</p>
              </div>

              <div className="flex-fill">
                <label htmlFor="brand" className="d-block">
                  {t("brand")}
                </label>
                <input {...register("brand")} />
                <p className="text-danger">{errors.brand?.message}</p>
              </div>
              <div className="flex-fill">
                <label htmlFor="type" className="d-block">
                  {t("type")}
                </label>
                <input {...register("type")} />
                <p className="text-danger">{errors.type?.message}</p>
              </div>
              <div className="flex-fill">
                <label htmlFor="serial" className="d-block">
                  {t("warranty-serial-number")}
                </label>
                <input {...register("serial")} />
                <p className="text-danger">{errors.serial?.message}</p>
              </div>
              <div className="flex-fill">
                <label htmlFor="complaint" className="d-block">
                  {t("complaint-text")}
                </label>
                <textarea
                  className="w-100"
                  {...register("complaint")}
                  rows={4}
                />
                <p className="text-danger">{errors.complaint?.message}</p>
              </div>

              <button
                className="btn btn-success w-lg-100 w-25"
                disabled={isLoading}
              >
                {t("submit")}
              </button>
            </form>
          </div>
        </div>
      </section>
    </div>
  );
}
export default Complaint;
