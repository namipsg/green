"use client";
import React from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";

function CoolingLoadCalculator() {
  const { t } = useTranslation("coolingLoadCalculator");
  const schema = yup
    .object({
      region: yup.string().required(t("region-select")),
      building: yup.string().required(t("user-type")),
      length: yup.number().min("0").required(t("room-length")),
      width: yup.number().required(t("room-width")),
      height: yup.number().required(t("room-height")),
      area: yup.number().required(),
      position: yup.string().required(t("specify-room-location")),
      person: yup.number().required(t("people-number")),
      floor: yup.string().required(t("floor-required")),
      capacity: yup.number().required(),
      power: yup.number().required(),
    })
    .required();
  const {
    register,
    // handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  return (
    <>
      <section>
        <div className="top-foroosh">
          <div className="container">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">
                    {t("cooling-load-calculation")}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="container cal-sec mx-auto p-5">
        <form
          className="d-flex flex-column gap-3 rounded border"
          style={{
            background: "whitesmoke",
            padding: "3%",
          }}
        >
          <div className="w-100 d-flex flex-column flex-lg-row gap-2 align-items-center">
            <p className="m-0">{t("house-room-specifications")}</p>
            <hr className="flex-fill" />

          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <FormControl className="flex-fill">
              <InputLabel id="province-label">{t("choose-region")}</InputLabel>
              <Select
                labelId="province-label"
                id="province-select"
                label={t("choose-region")}
                placeholder={t("choose-region")}
                {...register("region")}
                sx={{
                  background: '#fff'
                }}
              >
                <MenuItem
                  sx={{ color: "black" }}
                  value={1}
                >
                  {t("moderate")}
                </MenuItem>
                <MenuItem
                  sx={{ color: "black" }}
                  value={2}
                >
                  {t("hot-dry")}
                </MenuItem>
                <MenuItem
                  sx={{ color: "black" }}
                  value={3}
                >
                  {t("cold-mountainous")}
                </MenuItem>

              </Select>
            </FormControl>
            <FormControl className="flex-fill">
              <InputLabel id="province-label">{t("choose-username")}</InputLabel>
              <Select
                labelId="province-label"
                id="province-select"
                label={t("choose-username")}
                placeholder={t("choose-username")}
                {...register("building")}
                sx={{
                  background: '#fff'
                }}
              >
                <MenuItem
                  sx={{ color: "black" }}
                  value={1}
                >
                  {t("residential")}
                </MenuItem>
                <MenuItem
                  sx={{ color: "black" }}
                  value={2}
                >
                  {t("commercial")}
                </MenuItem>
              </Select>
            </FormControl>
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <TextField
                variant="outlined"
                label={t("length")}
                placeholder={t("length")}
                {...register("length")}
                type="number"
                fullWidth
                error={Boolean(errors.length?.message)}
                helperText={errors.length?.message}
                sx={{
                  background: "#fff"
                }}
              />
            </div>
            <div className="flex-fill">
              <TextField
                variant="outlined"
                label={t("width")}
                placeholder={t("width")}
                {...register("width")}
                type="number"
                fullWidth
                error={Boolean(errors.width?.message)}
                helperText={errors.width?.message}
                sx={{
                  background: "#fff"
                }}
              />

            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <FormControl fullWidth>
                <InputLabel id="province-label">{t("height")}</InputLabel>
                <Select
                  labelId="province-label"
                  id="province-select"
                  label={t("height")}
                  placeholder={t("height")}
                  {...register("height")}
                  sx={{
                    background: '#fff'
                  }}
                >
                  <MenuItem
                    sx={{ color: "black" }}
                    value={'U3'}
                  >
                    {t("up-to-three-meters")}
                  </MenuItem>
                  <MenuItem
                    sx={{ color: "black" }}
                    value={'3M'}
                  >
                    {t("three-meter")}
                  </MenuItem>
                  <MenuItem
                    sx={{ color: "black" }}
                    value={'M3'}
                  >
                    {t("more-than-three-meters")}
                  </MenuItem>

                </Select>
              </FormControl>
            </div>
          </div>
          <div className="w-100 d-flex flex-column flex-lg-row gap-2 align-items-center">
            <p className="m-0">{t("window-specifications")}</p>
            <hr className="flex-fill" />
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <TextField
                variant="outlined"
                label={t("total-area-windows")}
                placeholder={t("total-area-windows")}
                {...register("area")}
                type="number"
                fullWidth
                error={Boolean(errors.area?.message)}
                helperText={errors.area?.message}
                sx={{
                  background: "#fff"
                }}
              />
            </div>
            <div className="flex-fill">
              <FormControl fullWidth>
                <InputLabel id="province-label">{t("room-location")}</InputLabel>
                <Select
                  labelId="province-label"
                  id="province-select"
                  label={t("room-location")}
                  placeholder={t("room-location")}
                  {...register("position")}
                  sx={{
                    background: '#fff'
                  }}
                >
                  <MenuItem
                    sx={{ color: "black" }}
                    value={'NE'}
                  >
                    {t("north-east")}
                  </MenuItem>
                  <MenuItem
                    sx={{ color: "black" }}
                    value={'SW'}
                  >
                    {t("south-west")}
                  </MenuItem>

                </Select>
              </FormControl>
            </div>
          </div>
          <div className="w-100 d-flex flex-column flex-lg-row gap-2 align-items-center">
            <p className="m-0">{t("other-specifications")}</p>
            <hr className="flex-fill" />
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">

            <div className="flex-fill">
              <TextField
                variant="outlined"
                label={t("number-people-room")}
                placeholder={t("number-people-room")}
                {...register("person")}
                type="number"
                fullWidth
                error={Boolean(errors.person?.message)}
                helperText={errors.person?.message}
                sx={{
                  background: "#fff"
                }}
              />
            </div>
            <div className="flex-fill">
              <FormControl fullWidth>
                <InputLabel id="province-label">{t("floor")}</InputLabel>
                <Select
                  labelId="province-label"
                  id="province-select"
                  label={t("floor")}
                  placeholder={t("floor")}
                  {...register("floor")}
                  sx={{
                    background: '#fff'
                  }}
                >
                  <MenuItem
                    sx={{ color: "black" }}
                    value={0}
                  >
                    {t("middle")}
                  </MenuItem>
                  <MenuItem
                    sx={{ color: "black" }}
                    value={1}
                  >
                    {t("first")}
                  </MenuItem>

                </Select>
              </FormControl>
            </div>
          </div>
          <div className="w-100 d-flex flex-column flex-lg-row gap-2 align-items-center">
            <p className="m-0">{t("capacity-power-required")}</p>
            <hr className="flex-fill" />
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <label htmlFor="capacity" className="d-block mb-2">OBTU</label>
              <input
                placeholder="OBTU"
                type="number"
                style={{
                  paddingTop: "5px",
                  paddingBottom: "5px",
                  borderRadius: "5px",
                  border: "var(--bs-border-width) solid var(--bs-border-color)",
                }}
                className="w-100"
                readOnly
              />
            </div>
            <div className="flex-fill">
              <label htmlFor="power" className="d-block mb-2">
                {t("amount-required-power")}
              </label>
              <input
                placeholder="0.0KW"
                type="number"
                className="w-100"
                style={{
                  paddingTop: "5px",
                  paddingBottom: "5px",
                  borderRadius: "5px",
                  border: "var(--bs-border-width) solid var(--bs-border-color)",
                }}
              />
            </div>
          </div>
        </form>
      </section>
    </>
  );
}
export default CoolingLoadCalculator;
