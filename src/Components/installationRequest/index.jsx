"use client";
import React from "react";
import { useState } from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import Config from "../../../config/default";
import { DatePicker } from "zaman";
import { enqueueSnackbar } from "notistack";
import { useTranslation } from "react-i18next";

function InstallationRequest({ states }) {
  const { t } = useTranslation("installation-request");
  const { t: et } = useTranslation("error");
  const schema = yup
    .object({
      firstname: yup
        .string()
        .min(3, t("firstname-min"))
        .required(t("firstname-required")),
      lastname: yup
        .string()
        .min(3, t("lastname-min"))
        .required(t("lastname-required")),
      nationalCode: yup
        .string()
        .required(t("nationalCode-required"))
        .min(10, t("nationalCode-min"))
        .max(10, t("nationalCode-max")),
      birthDate: yup.date().required(t("Date-birth-required")),
      type: yup.string().required(t("request-type-required")),
      mobileNumber: yup
        .string()
        .required(t("mobileNumber-required"))
        .min(11, t("mobileNumber-min"))
        .max(11, t("mobileNumber-max")),
      phoneNumber: yup
        .string()
        .required(t("phoneNumber-required"))
        .min(11, t("phoneNumber-min"))
        .max(11, t("phoneNumber-max")),
      locationId: yup.number().required(t("province-required")),
      address: yup.string().required(t("address-required")),
      model: yup.string().required(t("model-required")),
      serialType: yup.string().required(t("serial-required")),
      serial: yup.string().required(t("serial-number-required")),
      storeName: yup.string(),
      trackCode: yup.string(),
      description: yup.string(),
    })
    .required();
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (formData) => {
    try {
      setIsLoading(true);
      const response = await fetch(Config.API.INSTALLATION.POST, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      const data = await response.json();
      if (response.status !== 200) {
        enqueueSnackbar(
          data.message?.[0] || et("installation-registration-error"),
          { variant: "error" }
        );
        setIsLoading(false);
      } else {
        enqueueSnackbar(et("installation-registration-success"), {
          variant: "success",
        });
        reset();
        setIsLoading(false);
      }
    } catch (error) {
      enqueueSnackbar(error.message || et("installation-registration-error"), {
        variant: "error",
      });
      setIsLoading(false);
    }
  };

  const [isLoading, setIsLoading] = useState(false);
  return (
    <>
      <section>
        <div className="top-pic-aftersale">
          <div className="container">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">
                    {t("installation-request-form")}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="container mx-auto p-5">
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="d-flex flex-column gap-3 install-form-div"
          style={{
            borderRadius: "10px",
            padding: "3%",
          }}
        >
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <label htmlFor="firstname" className="d-block m-2">
                {t("first-name")}
              </label>
              <input {...register("firstname")} />
              <p className="text-danger">{errors.firstname?.message}</p>
            </div>
            <div className="flex-fill">
              <label htmlFor="lastname" className="d-block m-2">
                {t("last-name")}
              </label>
              <input {...register("lastname")} />
              <p className="text-danger">{errors.lastname?.message}</p>
            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <label htmlFor="nationalCode" className="d-block m-2">
                {t("national-code")}
              </label>
              <input {...register("nationalCode")} />
              <p className="text-danger">{errors.nationalCode?.message}</p>
            </div>
            <div className="flex-fill">
              <label htmlFor="birthDate" className="d-block m-2">
                {t("date-of-birth")}
              </label>
              <DatePicker
                onChange={(date) => setValue("birthDate", new Date(date.value))}
              />
              <p className="text-danger">{errors.birthDate?.message}</p>
            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <label htmlFor="type" className="d-block m-2">
                {t("type-retquest")}
              </label>
              <div>
                <label htmlFor="field-rain" className="w-100">
                  <input
                    {...register("type")}
                    type="radio"
                    value={t("seasonal-service")}
                    id="field-service"
                    style={{
                      minWidth: "fit-content",
                      width: "fit-content",
                    }}
                  />
                  <span className="me-2">{t("seasonal-service")}</span>
                </label>
                <label className="w-100" htmlFor="field-fix">
                  <input
                    {...register("type")}
                    type="radio"
                    value={t("repair")}
                    id="field-fix"
                    style={{
                      minWidth: "fit-content",
                      width: "fit-content",
                    }}
                  />
                  <span className="me-2">{t("repair")}</span>
                </label>
                <label htmlFor="field-install" className="w-100">
                  <input
                    {...register("type")}
                    type="radio"
                    value={t("installation")}
                    id="field-install"
                    style={{
                      minWidth: "fit-content",
                      width: "fit-content",
                    }}
                  />
                  <span className="me-2">{t("installation")}</span>
                </label>
              </div>
              <p className="text-danger">{errors.type?.message}</p>
            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <label htmlFor="mobileNumber" className="d-block m-2">
                {t("mobile")}
              </label>
              <input {...register("mobileNumber")} />
              <p className="text-danger">{errors.mobileNumber?.message}</p>
            </div>
            <div className="flex-fill">
              <label htmlFor="phoneNumber" className="d-block m-2">
                {t("phone-number")}
              </label>
              <input {...register("phoneNumber")} />
              <p className="text-danger">{errors.phoneNumber?.message}</p>
            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="col-3">
              <label className="m-2">{t("province")}</label>
              <select {...register("locationId")}>
                {states?.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.name}
                  </option>
                ))}
              </select>
              <p className="text-danger">{errors.locationId?.message}</p>
            </div>
            <div className="flex-fill">
              <label htmlFor="address" className="d-block m-2">
                {t("address")}
              </label>
              <input {...register("address")} />
              <p className="text-danger">{errors.address?.message}</p>
            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <label htmlFor="model" className="d-block m-2">
                {t("model")}
              </label>
              <input {...register("model")} />
              <p className="text-danger">{errors.model?.message}</p>
            </div>
            <div className="col-12 col-md-2">
              <label htmlFor="serialType" className="d-block m-2">
                {t("serial")}
              </label>
              <div >
                <label
                  htmlFor="field-internal-unit"
                  className="w-100 d-flex align-items-center"
                >
                  <input
                    {...register("serialType")}
                    type="radio"
                    value={t("indoor-unit-serial")}
                    id="field-service"
                    style={{
                      minWidth: "fit-content",
                      width: "fit-content",
                    }}
                  />
                  <span className="me-2">{t("indoor-unit-serial")}</span>
                </label>
                <label
                  className="w-100 d-flex align-items-center"
                  htmlFor="field-external-unit"
                >
                  <input
                    {...register("serialType")}
                    type="radio"
                    value={t("outdoor-unit-serial")}
                    id="field-external-unit"
                    style={{
                      minWidth: "fit-content",
                      width: "fit-content",
                    }}
                  />
                  <span className="me-2">{t("outdoor-unit-serial")}</span>
                </label>
              </div>
              <p className="text-danger">{errors.serialType?.message}</p>
            </div>
            <div className="flex-fill">
              <label htmlFor="serial" className="d-block m-2">
                {t("serial-number")}
              </label>
              <input {...register("serial")} />
              <p className="text-danger">{errors.serial?.message}</p>
            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row gap-3">
            <div className="flex-fill">
              <label htmlFor="storeName" className="d-block m-2">
                {t("purchased-store-name")}
              </label>
              <input {...register("storeName")} />
              <p className="text-danger">{errors.storeName?.message}</p>
            </div>
            <div className="flex-fill">
              <label htmlFor="trackCode" className="d-block m-2">
                {t("tracking-code")}
              </label>
              <input {...register("trackCode")} />
              <p className="text-danger">{errors.trackCode?.message}</p>
            </div>
          </div>
          <div className="w-100" style={{ width: "97%" }}>
            <label htmlFor="description" className="d-block m-2">
              {t("description")}
            </label>
            <textarea className="w-100" {...register("description")} rows={4} />
            <p className="text-danger">{errors.description?.message}</p>
          </div>
          <button
            className="btn btn-success w-lg-100 w-25"
            disabled={isLoading}
          >
            {t("submit")}
          </button>
        </form>
      </section>
    </>
  );
}
export default InstallationRequest;
