"use client";
import * as React from "react";
import { useState } from "react";
import "./index.css";
import {
  Box,
  Select,
  FormControl,
  MenuItem,
  InputLabel,
  TextField,
  InputAdornment,
} from "@mui/material";
import _ from "lodash";
import { useTranslation } from "react-i18next";
import SearchIcon from "@mui/icons-material/Search";
import { BASEURL } from "../../../config/default";
import MapComponent from "./map";

const NamayandegiForoshContent = ({ states, type, label }) => {
  const { t } = useTranslation("namayandegi-forosh-content");
  const { t: et } = useTranslation("error");

  const [state, setState] = useState([]);
  const [cities, setCities] = useState([]);
  const [companies, setCompanies] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [city, setCitiy] = useState();
  const [originalData, setOriginalData] = useState();
  const [cordinates, setCordinates] = useState([]);
  const formParent = React.useRef();

  const handleStatesChange = async (e) => {
    setState(e.target.value);
    const selectedState = e.target.value;
    let response = await fetch(
      BASEURL + `/api/location/city?stateId=${selectedState}`,
      {
        cache: "no-store",
      }
    );
    const data = await response.json();
    setCities(data);
  };
  const handleCitiesChange = async (e) => {
    setIsLoading(true);
    const selectedCity = e.target.value;
    setCitiy(selectedCity);
    let response = await fetch(
      BASEURL + `/api/sale/get-all?cityId=${selectedCity}&type=${type}`,
      {
        cache: "no-store",
      }
    );
    const data = await response.json();
    setCompanies(data.data);
    if (data.data.length > 0) {
      let cords = [];
      data?.data?.map((item) => {
        console.log({
          label: item.name,
          cords: [item.latitude, item.longitude],
        });
        cords.push({
          label: item.name,
          cords: [item.latitude, item.longitude],
        });
      });

      setCordinates([...cords]);
    }
    setOriginalData(data.data);
    setIsLoading(false);
  };

  const handleSearch = async (e) => {
    let _data = _.cloneDeep(originalData);
    const filteredData = _data.filter((x) => x.name.includes(e.target.value));
    setCompanies(filteredData);
  };

  return (
    <>
      <section>
        <div className="top-foroosh">
          <div className="container ">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">{label}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="pad-50">
        <div className="namayandeh">
          <div className="container">
            <div className="d-flex flex-column flex-lg-row gap-3 align-items-start">
              <div className="col-md-6" ref={formParent}>
                <Box
                  sx={{
                    minWidth: 120,
                    marginBottom: "5%",
                  }}
                >
                  <FormControl fullWidth>
                    <InputLabel id="province-label">{t("province")}</InputLabel>
                    <Select
                      labelId="province-label"
                      id="province-select"
                      value={state}
                      label={t("province")}
                      placeholder={t("province")}
                      onChange={handleStatesChange}
                    >
                      {states?.map((state) => {
                        return (
                          <MenuItem
                            sx={{ color: "black" }}
                            value={state.id}
                            key={"ostan_" + state.id}
                          >
                            {state.name}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                </Box>
                <Box sx={{ minWidth: 120 }}>
                  <FormControl fullWidth>
                    <InputLabel id="city-label"> {t("city")} </InputLabel>
                    <Select
                      labelId="city-label"
                      id="city-select"
                      value={city}
                      disabled={cities.length < 1 || isLoading == true}
                      placeholder={t("city")}
                      label={t("city")}
                      onChange={handleCitiesChange}
                    >
                      {cities.data?.map((city) => {
                        return (
                          <MenuItem
                            sx={{ color: "black" }}
                            value={city.id}
                            key={"shahr_" + state.id}
                          >
                            {city.name}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                </Box>
                <TextField
                  variant="outlined"
                  id="green-search-representatives"
                  className="w-100 my-3"
                  type="search"
                  placeholder={t("search-representatives")}
                  onChange={handleSearch}
                  disabled={isLoading || !city}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <SearchIcon />
                      </InputAdornment>
                    ),
                  }}
                />
                <div className="list">
                  <ul class="list-group list-ul">
                    {companies?.length < 1 && city && isLoading == false
                      ? et("representative-not-found")
                      : [
                          city && isLoading == true
                            ? et("loading")
                            : !city
                            ? null
                            : companies?.map((company) => {
                                return (
                                  <li
                                    class="list-group-item list-li"
                                    key={"namayandegi_" + state.id}
                                  >
                                    <div>
                                      <p className="font-800">{company.name}</p>
                                    </div>
                                    <div>
                                      <p className="font-200">
                                        {company.address}
                                      </p>
                                    </div>
                                    <div className="namayande-phone-box">
                                      <phone className="namayande-phone">
                                        {company.phoneNumber} -{" "}
                                        {company.mobileNumber}
                                      </phone>
                                    </div>
                                  </li>
                                );
                              }),
                        ]}
                  </ul>
                </div>
              </div>
              <div className="col-md-6 h-100 relative border border-2 rounded-2">
                <MapComponent
                  center={[35.7219, 51.3347]}
                  zoom={10}
                  height={formParent?.current?.scrollHeight || 725}
                  cordinates={cordinates}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default NamayandegiForoshContent;
