'use client'
// src/MapComponent.jsx
import React, { useEffect, useMemo, useState } from 'react';
import { MapContainer, TileLayer, Marker, Popup, useMap } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

// Fix for missing marker icons with Webpack
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-icon-2x.png',
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-icon.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
});

// Marker data
const markers = [
    { id: 1, position: [51.505, -0.09], popup: "Marker 1" },
    { id: 2, position: [51.51, -0.1], popup: "Marker 2" },
    { id: 3, position: [51.51, -0.12], popup: "Marker 3" },
];

const ChangeView = ({ center, zoom }) => {
    const map = useMap();
    map.setView(center, zoom);
    return null;
};

const calculateCenter = (markers) => {
    const latitudes = markers.map(marker => marker.cords[0]);
    const longitudes = markers.map(marker => marker.cords[1]);

    const avgLat = latitudes.reduce((a, b) => a + b, 0) / latitudes.length;
    const avgLng = longitudes.reduce((a, b) => a + b, 0) / longitudes.length;

    return [avgLat, avgLng];
};

const FitBounds = ({ bounds }) => {
    const map = useMap();
    useEffect(() => {
        if (bounds) {
            map.fitBounds(bounds);
        }
    }, [map, bounds]);
    return null;
};


const MapComponent = ({ zoom, height, cordinates }) => {
    let center = [35.7219, 51.3347]
    const [bounds, setBounds] = useState(null);

    useEffect(() => {
        if (cordinates.length > 0) {
            const latLngs = cordinates.map(marker => marker.cords);

            const bounds = L.latLngBounds(latLngs);
            setBounds(bounds);
        }
    }, [cordinates]);

    if (!cordinates || cordinates.length < 1) {
        return <MapContainer center={center} zoom={zoom} style={{ height: "100%", minHeight: height, width: "100%", zIndex: 1 }}>
            <ChangeView center={center} zoom={zoom} />
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            />
        </MapContainer>
    }

    return (
        <MapContainer bounds={bounds} style={{ height: "100%", minHeight: height, width: "100%", zIndex: 1 }}>
            {bounds && <FitBounds bounds={bounds} />}
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            />
            {cordinates?.map(marker => (
                <Marker key={marker.id} position={marker.cords}>
                    <Popup>{marker.label}</Popup>
                </Marker>
            ))}
        </MapContainer>
    );

};

export default MapComponent;
