"use client";
import "./index.css";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Navigation, Pagination } from "swiper/modules";
import { useTranslation } from "react-i18next";
import "swiper/css";
import "swiper/css/navigation";
import { PANELURL } from "../../../config/default";

export default function ProjectsContent({ finalData }) {
  const { t } = useTranslation("projects");
  const { data } = finalData;

  return (
    <>
      <section>
        <div className="top-pic">
          <div className="container ">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">{t("projects")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div> 
      </section>
      <section>
        <div className="Projects-slider">
          <div className="container ">
            <Swiper
              dir="rtl"
              slidesPerView={2}
              spaceBetween={30}
              autoplay={{
                delay: 2500,
                disableOnInteraction: false,
              }}
              pagination={{
                clickable: true,
              }}
              breakpoints={{
                200: {
                  slidesPerView: 1,
                  spaceBetween: 50,
                },
                768: {
                  slidesPerView: 2,
                  spaceBetween: 50,
                },
              }}
              modules={[Autoplay, Pagination]}
            >
              {data?.map((item) => (
                <SwiperSlide className="swiperslide h-100" key={item.id}>
                  <div className="w-100 d-flex flex-column">
                    <div className="slider-box-pic w-100 mb-auto">
                      <img
                        src={PANELURL + item.image}
                        className="img-fluid w-100 rounded-2 border"
                        style={{
                          minWidth: 200,
                        }}
                        alt={item.name}
                      />
                    </div>
                    <div className="mt-3 project-name__catagory">
                      <h4 className="mb-0">{item.name}</h4>
                      <p className="m-0">{item.categoryName}</p>
                    </div>
                    <h5 className="m-0">{item.state}</h5>
                  </div>
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
        </div>
      </section>
    </>
  );
}
