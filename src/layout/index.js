const { default: Footer } = require("@/Components/Footer");
const { default: TopNav } = require("@/Components/Nav/TopNav");

const Layout = ({ children }) => {
  return (
    <>
      <TopNav />
      {children}
      <Footer />
    </>
  );
};

export default Layout;
