/** @type {import('next').NextConfig} */
const nextConfig = {};
const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");
// const { webpack } = require("next/dist/compiled/webpack/webpack");

module.exports = {
  nextConfig,
  webpack: (config) => {
    config.plugins.push(
      new LodashModuleReplacementPlugin({
        // shorthands:false,
        // cloning:false,
        modularize: true,
      })
    );
    return config;
  },
};
