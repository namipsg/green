export const BASEURL = "http://85.208.253.36:8081";
export const PANELURL = "http://85.208.253.36:8082";
const Config = {
  API: {
    CATEGORY: {
      GETALL: BASEURL + "/api/category/get-all",
    },
    NEWS: {
      GETALL: BASEURL + "/api/news/get-all",
      GET: BASEURL + "/api/news/get",
      COMMENT: BASEURL + "/api/news/comment",
    },
    LOCATION: {
      STATE: BASEURL + "/api/location/state",
      CITY: BASEURL + "/api/location/city",
    },
    SALE: {
      GETALL: BASEURL + "/api/sale/get-all",
    },
    GALLERY: {
      GROUP: BASEURL + "/api/gallery/group",
      GETLEVEL: BASEURL + "/api/gallery/level2",
      GET: BASEURL + "/api/gallery/get",
    },
    PROJECT: {
      GETALL: BASEURL + "/api/project/get-all",
      GET: BASEURL + "/api/project/get",
    },
    MENU: {
      GET: BASEURL + "/api/menu/get-all",
    },
    PRODUCT: {
      GETALL: BASEURL + "/api/product/get-all",
      GET: BASEURL + "/api/product/get",
    },
    COMPLAINT: {
      POST: BASEURL + "/api/complaint/insert",
    },
    INSTALLATION: {
      POST: BASEURL + "/api/installtion/insert",
    },
    HOME: {
      GET: BASEURL + "/api/home",
    },
    UNIT: {
      GET: BASEURL + "/api/convertor/get-type",
      GETSUBS: BASEURL + "/api/convertor/get-all",
      CONVERT: BASEURL + "/api/convertor/get",
    },
    SEARCH: {
      GET: BASEURL + "/api/search",
    },
  },
};
export default Config;
